<?php /*
* file:		Edit.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.02.2018
*/ 

	ini_set('display_errors', true); // !!! remove after DEBUG !!!
	include_once 'ENGINE/Site.php';

	if (Site::getLogInStatus()) {
		include_once 'TEMPLATE/editor.php';
	} else {
		Site::Redirect('/login.php');
	}

 ?>