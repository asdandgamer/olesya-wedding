<?php /*
* file:		item.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.02.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!

require_once 'ENGINE/Engine.php';
require_once 'ENGINE/ItemsManager.php';

$itemsManager = new ItemsManager();
$item = array();
$categoryInf = array();
$collectionInf = array();
if (isset($_GET['itemID'])) {
	$options = array(
      'options' => array(
          'default' => 0, // значение, возвращаемое, если фильтрация завершилась неудачей
          // другие параметры
          'min_range' => 0
      ),
      'flags' => FILTER_FLAG_ALLOW_OCTAL,
    );
    $var = filter_var($_GET['itemID'], FILTER_VALIDATE_INT, $options);
    if ($var == 0) {
      Site::Redirect('/404');
    }
    //----------------------------------------------------------
	$item = $itemsManager->GetItemInfo($_GET['itemID']);
	$collectionInf = $itemsManager->GetCollectionInfo($item[2]);
	array_push($categoryInf, $collectionInf[3]);
  	array_push($categoryInf, $itemsManager->GetCategopryName($categoryInf[0]));
}
$Title = $item[1] . ' | ' . Site::GetSiteName();
// Open Graph -------------------------------------------------------
require_once 'ENGINE/ImageManager.php';
$imgMan = new ImagesManager();
$imgID = explode(", ",$item[4])[0];

$description = 'Відмінна сукня '.$item[1].' з колекції '.$collectionInf[1];

$OG = array(
  'title' => $Title, 
  'url' => 'http://109.162.0.179/item.php?itemID='.$_GET['itemID'],
  'description' => $description,
  'image' => 'http://109.162.0.179/'.preg_replace("/[ ]/","%20", substr($imgMan->GetOriginal($imgID), 1))
);
//print_r($OG);
//-------------------------------------------------------------------
include_once 'TEMPLATE/header.php';	//header
include_once 'TEMPLATE/menu.php';	//menu
//-------------------------------------------------------------------
include_once 'TEMPLATE/item.php';
include_once 'TEMPLATE/footer.php';	//footer
?>