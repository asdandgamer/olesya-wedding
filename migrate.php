<?php /*
* file:		migrate.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		02.06.2018
*/

ini_set("display_errors", true);

require_once 'ENGINE/Engine.php';
if(!Site::getLogInStatus()) {
    Site::Redirect('/login.php');
} else if (Site::GetUserLevel()!=USR_LEVEL_ADMIN) {
    echo "Only administrator can use this feature";
} else {
    $db = new DB();
    $db->Connect();
    //---------------------------------------------------------------
    PrintMsg($db->ExecNoResult('ALTER TABLE usr ADD level TINYINT(1) UNSIGNED NOT NULL'));
    //---------------------------------------------------------------
    PrintMsg($db->ExecNoResult("UPDATE usr SET level='9' WHERE id='1' "));
}

function PrintMsg(bool $status)
{ 
    global $db;
    $str = '<span style="color: ';// . $status==true ? 'green' : 'red'; $str = $str . '">';
    if($status)
       $str = $str . 'green;">success: ' . $db->GetLastRequest();
    else
        $str = $str . 'red;">failed: ' . $db->GetLastError();
    //---------------------------------------------------------------
    $str = $str . '</span><br>';
    echo $str;
}

?>