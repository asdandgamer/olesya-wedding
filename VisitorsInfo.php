<?php /*
* file:		VisitorsInfo.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		22.02.2018
*/ 
require_once 'ENGINE/Engine.php';

// if (Site::ge) {
// 	# code...
// }

if (!Site::getLogInStatus()) {
	$db = new DB();
	$db->Connect();
	$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);

	$curDate = date("Y-m-d");
	$curTime = date("H:i:s");
	$at = $curDate . ' ' . $curTime;
	$browser = $_SERVER['HTTP_USER_AGENT'];
	$ipinfo = addslashes(shell_exec('curl ipinfo.io/'. $ip));
	$ref_link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'direct';

	$data = array($at, $browser, $ipinfo, $ref_link, $ip, $curDate);

	$db->InsertVal('visitsinfo', $data);
}
?>