<?php /* 
* -------------------------------------------------------------------
* file:		sitemap.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		08.02.2018
* -------------------------------------------------------------------
* Description: sitemap.xml dynamic generator.
* nginx config: 
FILE[/etc/nginx/sites-available/default] @ EOF
_____________________________________________________________________
	location = /sitemap.xml {
		try_files $uri /sitemap.php;
	}
_____________________________________________________________________
*/
require_once 'ENGINE/Engine.php';
$db = new DB();
$db->Connect();
//-------------------------------------------------------------------
$staticPages = array('about.php', 'video.php', 'contacts.php', 'suggestions.php', 'accesoires.php', 'ladies.php');
$categoriesPagesIDs = $db->Select('category', 'id', NULL);
$collectionPagesIDs = $db->Select('collection', 'id', NULL);
$itemsPagesIDs = $db->Select('item', 'id', NULL);
//-------------------------------------------------------------------
header("Content-Type: application/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . PHP_EOL;
/* ----  STAIC PAGES  -----  */
PrintUrl($staticPages, '/', 'daily');
/* ----  CATEGORIES   -----  */
PrintUrl($categoriesPagesIDs, '/category.php?CatID=', 'daily');
/* ----  COLLECTIONS  -----  */
PrintUrl($collectionPagesIDs, '/collection.php?CollID=', 'daily');
/* ----     ITEMS     -----  */
PrintUrl($itemsPagesIDs, '/item.php?itemID=', 'monthly');
//-------------------------------------------------------------------
echo '</urlset>' . PHP_EOL;

function PrintUrl($arr, $way, $changefreq)
{
	for ($i=0; $i < count($arr); $i++) { 
		echo '<url>' . PHP_EOL;
		if(is_array($arr[$i]))
			echo '<loc>' . 'http://'.Site::GetSiteAdress(). $way . $arr[$i][0] . '</loc>' . PHP_EOL;
		else
			echo '<loc>' . 'http://'.Site::GetSiteAdress(). $way . $arr[$i] . '</loc>' . PHP_EOL;
		echo '<changefreq>' . $changefreq . '</changefreq>' . PHP_EOL;
		echo '</url>' . PHP_EOL;
	}
}

?>