$(document).ready(function() {
	$('.image').each(function() {
		var imgID = $(this).attr('id');
		var target = $(this).children('img');
		GetFirstItemImg(imgID, target);
	});
});

function GetFirstItemImg(id, target) {
	$.ajax({
		url: "AJAX/Images.php",
		type: "GET",
		data: { id: id, size: 's400' },
		dataType: 'text',
		success: function(data) {
			$(target).attr('src', data);
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}