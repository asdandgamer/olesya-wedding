$(document).ready(function() {
	SendStatisticDataToServer();
})
function SendStatisticDataToServer() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(Position);
	} else {
		IpInfo(null);
	}
}
function Position(position) {
	var geoPosition = [position.coords.latitude, position.coords.longitude];
	IpInfo(geoPosition);
}
function IpInfo(geoPosition) {
	$.ajax({
		url: "https://ipinfo.io/json",
		type: "GET",
		dataType: "json",
		success: function(response) {
			SendStatisticData(response, geoPosition);
		},
		error: function(xhr) {
			console.error(xhr);
			SendStatisticData(null, geoPosition);
		}
		
	});

}
//{"ip":"194.44.135.39","city":"Ternopil","region":"Ternopil's'ka Oblast'","country":"UA","loc":"49.5559,25.6056","org":"AS3255 State"}
function SendStatisticData(ipinfo, geoPosition) {
	var Data; 
	if (ipinfo == null && Array.isArray(geoPosition)) {
		Data = {
			info: 1,
			ref: document.referrer,
			latitude: geoPosition[0],
			longitude: geoPosition[1]
		}
	} else if(ipinfo == null && !(Array.isArray(geoPosition))) {
		Data = {
			info: 3
			ref: document.referrer,
		}
	} else if(Array.isArray(geoPosition)) {
		Data = {
			info: 1,
			ref: document.referrer,
			city: ipinfo.city,
			region: ipinfo.region,
			country: ipinfo.country,
			latitude: geoPosition[0],
			longitude: geoPosition[1]
		}
	} else {
		Data = {
			info: 2,
			ref: document.referrer,
			city: ipinfo.city,
			region: ipinfo.region,
			country: ipinfo.country,
			loc: ipinfo.loc
		}
	}
	// $city, $region, $country, $provider,  $latitude, $longitude
	$.ajax({
		type: "POST",
		url: 'AJAX/Statistic.php',
		data: Data,
		dataType: "json",
		success: function(data) {
			console.log(JSON.stringify(data));
			// alert(JSON.stringify(data));
		},
		error: function(xhr) {
			console.error(xhr);
			$('.err_msg').html(xhr.responseText);
		}
	});
}