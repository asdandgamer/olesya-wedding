/*
* -------------------------------------------------------------------
* file:		SlideImg.js
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.02.2018
* -------------------------------------------------------------------
*/
var imagesIDs = JSON.parse($('#data').text());
//alert(Object.entries(imagesIDs));

var current = 0;
var imgs_count = imagesIDs.length;

function GetPreviews() {
	for (var i = 0; i < imgs_count; i++) {
		var li = document.createElement('li');
		li.className = 'img_prew';
		li.value = imagesIDs[i];
		var img = document.createElement('img');
		img.id = 'img_'+i+1;
		li.appendChild(img);
		$('#slider').append(li);
		appendImgSrcMin(imagesIDs[i], img.id);
	}
	appendFullImgSrc(imagesIDs[0]);
}
function appendFullImgSrc(imgID) {
	$.ajax({
		url: "AJAX/Images.php",
		type: "GET",
		data: { id: imgID, size: 'original' },
		dataType: 'text',
		success: function(data) {
			$('#main_img').children('img').attr('src', data);
			// SetSlideBtnsPos();
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function appendImgSrcMin(imgID, targetID) {
	$.ajax({
		url: "AJAX/Images.php",
		type: "GET",
		data: { id: imgID, size: 's200' },
		dataType: 'text',
		success: function(data) {
			$('#'+targetID).attr('src', data);
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function set_img_srcs()
{
	$('#slider').each(function(){
		$(this).find('li').each(function(){
			img_srcs.push($(this).children('img').attr('src'));
			imgs_count++;
		});
	});
}
function SetSlideBtnsPos() {
	var imgHeight = $('#main_img').height();
	var slideMargin = (imgHeight / 2) - 100;
	$('.image_slide_buttons_container').css('margin-top', slideMargin);
}

$(document).ready(function(){
	GetPreviews();
	// SetSlideBtnsPos();
	$(".img_prew").click(function(){
		appendFullImgSrc($(this).val());
		current = $(this).index();
	});
	$("#prev_btn").click(function(){
		current--;
		if (current < 0) { current = imgs_count-1; }
		appendFullImgSrc(imagesIDs[current]);
	});
	$("#next_btn").click(function(){
		current++;
		if (current == imgs_count) { current = 0;}
		appendFullImgSrc(imagesIDs[current]);
	});
});
// $(window).resize(function() {
// 	SetSlideBtnsPos();
// });