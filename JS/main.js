/*
file:	main.js
autor:	asdandgamer
*/
$(document).ready(function() {
	OnResize();
	$(".header_center").click(function(){
		$(location).attr('href', '/')
	});
});
$(window).resize(function() {
	OnResize();
});
function OnResize() {
	ResizeMap();
	ResizeVideo();
	ResizeCategoryImage();
	resizeItemImage();
	ResizeSlider();
	setHomeLinkPosition();
}
function ResizeVideo() {
	$(".videoFrame").width($(".content_wrap").width());
	$(".videoFrame").height($(".videoFrame").width() * 0.5625);
}
function ResizeCategoryImage() {
		var imgWidth=0;
	if($(".content_wrap").width() > 1000 ) { 
		var imgWidth = ($(".content_wrap").width()-77)/3; //86.7
	} else if ($(".content_wrap").width() > 640) {
		var imgWidth = ($(".content_wrap").width()-54)/2; //86.7
	} else {
		var imgWidth = $(".content_wrap").width()-30; //86.7
	}
	$(".image").css('width', imgWidth);
	$(".image").css('height', imgWidth);
	$(".image>img").width(imgWidth);
	$(".image>img").height(imgWidth);
}
function resizeItemImage() {
	var wWidth=$(window).width();
	if(wWidth < 1100) {
		var imgMaxWidth = wWidth - 30;
		$('#main_img').children('img').css('max-width', imgMaxWidth);
	} else {
		$('#main_img').children('img').css('max-width', '1070px');
	}
}
function ResizeSlider() {
	wWidth=$(window).width();
	if(wWidth < 1100) {
		var sliderWidth = wWidth-1;
		$('#slider').css('width', sliderWidth);
	} else {
		$('#slider').css('width', '852px');
	}
}
function setHomeLinkPosition() {
	wWidth=$(window).width();
	var left = (wWidth - 250) / 2;
	$('.header_center').css('left', left);
}
function ResizeMap() {
	wWidth=$(window).width();
	if(wWidth < 1100) {
		var mapMaxWidth = wWidth - 20;
		$('.map').children('iframe').width(mapMaxWidth); // = css('width', );
		$('.map').children('iframe').height(mapMaxWidth * 0.55);
		$('.map').height(mapMaxWidth * 0.55);
	} else {
		$('.map').css('max-width', '1080px');
	}
}
// function Add dropdown(argument) {
// 	// body...
// }