var imgCount;
var currentImgID;
var imagesIDs;

var FirstLoadPage;
var categoryIsEmpty;
var collectionIsEmpty;
var is_data_changed = true;

//------------------------------------
function ShowVisitors() {
	is_data_changed = false;
	$('.editor_content').css('display', 'none');
	$('.visitsinfo').css('display', 'table-cell');
}
function ShowEditor() {
	is_data_changed = true;
	$('.visitsinfo').css('display', 'none');
	$('.editor_content').css('display', 'table-cell');
}
function GoToMain() {
	$(location).attr('href', '/');
}
function GoToADMT() {
	$(location).attr('href', '/ADMT');
}
// ------------------------------

window.onbeforeunload = function () {
	if (!is_data_changed) {
		return;
	}
	return (is_data_changed ? "Измененные данные не сохранены. Закрыть страницу?" : null);
} 
$(document).ready(function() {
	imgCount=0;
	currentImgID=0;
	imagesIDs = new Array();
	categoryIsEmpty = true;
	collectionIsEmpty = true;
	FirstLoadPage = true;

	LoadCategories();
	$("#btn_close").click(function() {
		ClearUploadData();
		$('.owerdraw').hide();
	});
	$("#btn_add_image").click(function() {
		if(currentImgID != 0 && currentImgID !='undefined') {
			AddImg();
			NewImgTag(imagesIDs[imgCount-1][0], currentImgID);
		}
		ClearUploadData();
		$('.owerdraw').hide();
	});
});
$(window).resize(function() {
	SetInstrBackSize('.collection_editor_form', '.collection_editor');
	SetInstrBackSize('.add_img_form', '.owerdraw');
})
function SetInstrBackSize(elementClass, targetClass) {
	var hPadding = ($(window).height()-$(elementClass).height()) / 2 - 1;
	$(targetClass).css('padding', hPadding +'px 0px');
}
function AddCollection() {
	var formData = { 
		name: $('#coll_name').val(),
		year: $('#coll_year').val(),
		manufID: $('#manuf_sel').find(":selected").val(),
		CategID: $('#catg_sel').find(":selected").val(),
		CollIMG: $('#coll_prev_image').val()
	}
	$.ajax({
		type: "POST",
		url: 'AJAX/Items.php',
		data: formData,
		dataType: "json",
		success: function(data) {
			// alert(JSON.stringify(data));
			ClearCollEditor();
			CloseCollectionEditor();
			LoadCollections();
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function AddItem() {
	var itemName = $('#item_name').val();
	if(itemName == '') { alert('Поле імені пусте!'); return; }
	//---------------------------------------------------------------
	var price = $('#item_price').val();
	if(price == '') price=0;
	//---------------------------------------------------------------
	var images = new Array();
	for (var i = 0; i < imagesIDs.length; i++) {
		images.push(imagesIDs[i][1]);
	}
	//var colors = new Array();
	var ItemData = {
		name: itemName,
		price: price,
		collectionID: $('#collections').find(":selected").val(),
		imagesID: images,
	}
	$.ajax({
		type: "POST",
		url: 'AJAX/Items.php',
		data: ItemData,
		dataType: "json",
		success: function(data) {
			// alert(JSON.stringify(data));
			alert('Додано. Обновіть сторінку.');
			is_data_changed = false;
		},
		error: function(xhr) {
			console.error(xhr);
			alert('Під час додання сталася помилка. Опис помилки:' + xhr.responseText);
		}
	});
}
function LoadCollectionInfo() {
	$('#man_sp').text('');
	$('#year_sp').text('');
	if(collectionIsEmpty) { alert('Please select collection!!!'); return; }
	var collID = $('#collections').find(":selected").val();
	if(!collID) { console.log('no collections to load' + collID); return;}
	$.ajax({
		type: "GET",
		url: "AJAX/Items.php",
		dataType: "json",
		data: { type: 'collections', collectionID: collID },
		success: function(data) {
			//alert(Object.entries(data)+ Object.values(data));
			$('#man_sp').text(data.manufacturer);
			$('#year_sp').text(data.year);
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function LoadManufacturers() {
	$.ajax({
		type: "GET",
		url: "AJAX/Items.php",
		dataType: "json",
		data: { type: 'manufacturers' },
		success: function(data) {
			$('#manuf_sel').empty();
			for (var i = 0; i < data.length; i++) {
				var opt = document.createElement('option');
				opt.value = (Object.values(data))[i][0];
				opt.innerHTML = (Object.values(data))[i][1];
				$('#manuf_sel').append(opt);
			}
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function LoadCategoriesToCollEditor() {
	$.ajax({
		type: "GET",
		url: "AJAX/Items.php",
		dataType: "json",
		data: { type: 'categories' },
		success: function(data) {
			$('#catg_sel').empty();
			for (var i = 0; i < data.length; i++) {
				var opt = document.createElement('option');
				opt.value = (Object.values(data))[i][0];
				opt.innerHTML = (Object.values(data))[i][1];
				$('#catg_sel').append(opt);
			}
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function LoadCategories() {
	if (!categoryIsEmpty) { LoadCollections(); return; }
	$.ajax({
		type: "GET",
		url: "AJAX/Items.php",
		dataType: "json",
		data: { type: 'categories' },
		success: function(data) {
			categoryIsEmpty = false;
			$('#categories').empty();
			for (var i = 0; i < data.length; i++) {
				var opt = document.createElement('option');
				opt.value = (Object.values(data))[i][0];
				opt.innerHTML = (Object.values(data))[i][1];
				$('#categories').append(opt);
			}	
			if(FirstLoadPage) { LoadCollections(); };
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function LoadCollections() {
	collectionIsEmpty = false;
	var curCat = $('#categories').find(":selected").val();
	if(curCat == '') { alert('Please select category!!!!'); return; }
	$.ajax({
		type: "GET",
		url: "AJAX/Items.php",
		dataType: "json",
		data: { type: 'collections', category: curCat },
		success: function (data) {
			//alert(JSON.stringify(data));
			// for (var i = Things.length - 1; i >= 0; i--) {
			// 	Things[i]
			// }
			//alert(Object.entries(data)+ Object.values(data));
			
			$('#collections').empty();
			//var arr = $.map(data, function(el) { return el });
			//Object.keys(data).map(function(index) { return data[index]; })
			for (var i = 0; i < data.length; i++) {
				var opt = document.createElement('option');
				opt.value = (Object.values(data))[i][0];
				opt.innerHTML = (Object.values(data))[i][1];
				$('#collections').append(opt);
			}
			LoadCollectionInfo();
			if(FirstLoadPage) { FirstLoadPage = false; };
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
}
//--------------------------------
function ClearCollEditor() {
	$('#coll_name').val('');
	$('#coll_year').val('');
}
function ClearUploadData() {
	currentImgID=0;
	$('#load_img').attr('src', '');
	$('#load_img').hide();
	$('.myprogress').text('0%');
	$('#myfile').val('');
}
function Upload() {
	var myfile = $('#myfile').val();
	var formData = new FormData();
	formData.append('myfile', $('#myfile')[0].files[0]);
	
	$.ajax({
		url: 'AJAX/Images.php',
		data: formData,
		processData: false,
		contentType: false,
		type: 'POST',
		// this part is progress bar
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);
					$('.myprogress').text(percentComplete + '%');
					$('.myprogress').css('width', percentComplete + '%');
					if(percentComplete == 100) { 
						$('#load_img').show(); 
						$('#load_img').attr('src', '../IMG/load.gif'); 
					}
				}
			}, false);
			return xhr;
		},
		success: function (data) {
			// alert(JSON.stringify(data));
			result = JSON.parse(data);
			currentImgID = result.id;
			$('#load_img').attr('src', result.link);
		}
	});
}
// function get img link throw ajax, and create new tag in div with id img_<id>
function NewImgTag(tagID, id) {
	$.ajax({
		type: "GET",
		url: 'AJAX/Images.php',
		dataType: "text",
		data: { id: id, size: 's200'},
		success: function(data) {
			$('#' + tagID).prepend($('<img>',{id: tagID +'i',src:data}));
		}, 
		error: function(xhr) {
			console.log(xhr);
		}
	});
}
function NewImgDiv(id) {
	var newImgDiv = document.createElement("div");
	newImgDiv.className = 'image';
	newImgDiv.onclick = RemoveImg;
	newImgDiv.id = id;
	$(".images").append(newImgDiv);
}
function AddImg() {
	imgCount = imgCount + 1;
	var id = "img_" + imgCount;
	imagesIDs.push([id, currentImgID]);
	NewImgDiv(id);
}
function DisplayInstruments() {
	$('.owerdraw').show();
	SetInstrBackSize('.add_img_form', '.owerdraw');
}
function DisplayCollectionEditor() {
	$('.collection_editor').show();
	SetInstrBackSize('.collection_editor_form', '.collection_editor');
	LoadManufacturers();
	LoadCategoriesToCollEditor();
}
function CloseCollectionEditor() {
	//-----------
	$('.collection_editor').hide();
}
//-----------------------------------------------
function RemoveImg() {
	if (window.confirm('Remove Img?')) {
		var id = $(this).attr('id');
		for (var i = 0; i < imagesIDs.length; i++) {
			if(imagesIDs[i] == $(this).attr('id')) {
				array.splice(i, 1);
			}
		}
		$(this).remove();
	}
}