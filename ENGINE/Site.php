<?php
/*	FILE:	Site.php
* ________________________________
*
*	author:	asdandgamer
*	site:	asdandgamer.ucoz.ru
*	e-mail:	asdandgamer@gmail.com
* ________________________________
* 
*	DESCRIPTION: Contains Class that manage of site.
*/

define("USR_LEVEL_ADMIN", 9);
define("USR_LEVEL_MODER", 4);
define("USR_LEVEL_USER", 2);
define("USR_LEVEL_GUEST", 1);

ini_set("display_errors", true);
include_once 'DB.php';
//-------------------------------------------------------------------
session_start();

class Site 
{
	public function GetSiteName()
	{
		$Settings = parse_ini_file('.site_settings.ini');
		return $Settings["SITE_NAME"];
	}
	public function GetSiteAdress()
	{
		$Settings = parse_ini_file('.site_settings.ini');
		return $Settings["SITE_ADDR"];
	}
	//---------------------------------------------------------------
	public static function LogIn($usrName, $passwd)
	{
		if(self::getLogInStatus()) { self::LogOut(); }
		//------------------------------------------------------
		$db = new DB();
		$db->Connect();
		$result = $db->Select('usr', array('id', 'name', 'hash', 'level'), 'name=\''.$usrName.'\'');
		//-------------------------------------------------------
		if(count($result)!=1) return false;
		//-------------------------------------------------------
		$result = $result[0];
		if(($result[1] == $usrName) && ($result[2]==md5(md5($passwd).$usrName))) {
			self::setLogInStatus(true);
			self::setUserId($result[0]);
			self::setUserName($result[1]);
			self::SetCurrenUserLevel($result[3]);
			return true;
		} else {
			self::setLogInStatus(false);
			return false;
		}
	}
	//---------------------------------------------------------------
	public static function LogOut()
	{
		self::setUserName('');
		self::setLogInStatus(false);
		// remove all session variables
		session_unset(); 
		// destroy the session 
		session_destroy();
	}
	//--------STATUS ------------------------------------------------
	private static function setLogInStatus($status)
	{
		$_SESSION["isLogIn"] = $status;
	}
	public static function getLogInStatus()
	{
		if($_SESSION)
			return $_SESSION["isLogIn"];
		else
			return false;
	}
	public static function swapLogInStatus()
	{
		$_SESSION["isLogIn"] = !$_SESSION["isLogIn"];
	}
	//--------USER NAME----------------------------------------------
	public static function getUserName()
	{
		if($_SESSION)
			return $_SESSION["user"];
		else
			return '';
	}
	private static function setUserName($name)
	{
		if($name == NULL) return false;
		else $_SESSION["user"] = $name;
		return true;
	}
	//----USER ID *** -----------------------------------------------
	public static function getUserId()
	{
		if(array_key_exists("user_id",$_SESSION))
			return $_SESSION["user_id"];
		else
			return NULL;
	}
	private static function setUserId($userID)
	{
		$_SESSION["user_id"] = $userID;
	}
	//----USER LEVEL *** --------------------------------------------
	public static function GetUserLevelByName($usrName) {
		$db = new DB();
		$db->Connect();
		//-----------------------------------------------------------
		return $db->Select('usr', 'level', 'name=\''.$usrName.'\'')[0][0];
	}
	public static function GetUserLevelByID($userID) {
		$db = new DB();
		$db->Connect();
		//-----------------------------------------------------------
		return $db->Select('usr', 'level', 'id=\''.$userID.'\'')[0][0];
	}
	public static function GetCurrentUserLevel() {
		return $_SESSION["user_lvl"];
	}
	private static function SetCurrenUserLevel($level) {
		$_SESSION["user_lvl"] = $level;
	}
	//---------------------------------------------------------------
	public static function Redirect($url, $statusCode = 303)
	{
		header('Location: ' . $url, true, $statusCode);
		die();
	}
}
?>
