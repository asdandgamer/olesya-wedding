<?php /*
* file:		ItemsManager.php @ Engine
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		24.01.2018
*/ 

//-------------------------------------------------------------------

ini_set("display_errors", true);

include_once 'DB.php';
include_once 'ImageManager.php';

/* Current class manage items on site */
class ItemsManager
{
	private $imgMan;
	private $db;
	private $err_msg;
	
	function __construct()
	{
		$this->db = new DB();
		$this->imgMan = new ImagesManager();
		if($this->db->Connect())
			$this->SetErrMsg(false);
		else
			$this->SetErrMsg('connection failed: ' . $this->db->GetLastError());

		$this->db->SetCharset('utf8');
	}

	private function SetErrMsg($msg)
	{
		$this->err_msg = $msg;
	}

	public function ErrMsg()
	{
		return $this->err_msg;
	}

	// CATEGORIES  --------------------------------------------------
	public function AddCategopry($categoryName)
	{
		$lastID = $this->GetLastId('category');
		$this->db->InsertVal('category', array($lastID+1, $categoryName));
	}
	public function GetCategories()
	{
		$records = $this->db->Select('category');
		$categoriesList = array();
		for ($i=0; $i < count($records); $i++) { 
			array_push($categoriesList, array($records[$i][0], $records[$i][1]));
		}
		return $categoriesList;
	}
	public function GetCategoriesNames()
	{
		$records = $this->db->Select('category');
		$categoriesList = array();
		for ($i=0; $i < count($records); $i++) { 
			array_push($categoriesList, $records[$i][1]);
		}
		return $categoriesList;
	}
	public function GetCategopryName($id)
	{
		return ($this->db->Select('category', 'name', "id=".$id))[0][0];
	}
	//END CATEGORIES ------------------------------------------------

	// MANUFACTURERS ------------------------------------------------

	public function AddManufacturer($name)
	{
		$lastID = $this->GetLastId('manufacturer');
		$this->db->InsertVal('manufacturer', array($lastID+1, $name));
	}
	public function GetManufacturersWID() // WID - with ID
	{
		$records = $this->db->Select('manufacturer');
		$manufacturersList = array();
		for ($i=0; $i < count($records); $i++) { 
			array_push($manufacturersList, array($records[$i][0], $records[$i][1]));
		}
		return $manufacturersList;
	}
	public function GetManufacturers()
	{
		$records = $this->db->Select('manufacturer');
		$manufacturersList = array();
		for ($i=0; $i < count($records); $i++) { 
			array_push($manufacturersList, $records[$i][1]);
		}
		return $manufacturersList;
	}
	public function GetManufacturerName($id)
	{
		return ($this->db->Select('manufacturer', 'name', "id=".$id))[0][0];
	}
	public function RemoveManufacturer($mfgID) 
	{
		$this->db->Delete('manufacturer', 'id='.$mfgID);
	}

	//END MANUFACTURERS ---------------------------------------------

	// COLLECTIONS --------------------------------------------------
	public function AddCollection($name, $year, $categoryID, $manufacturerID, $imgLink=NULL)
	{
		$lastID = $this->GetLastId('collection');
		$this->db->InsertVal('collection', array($lastID+1, $name, $year, $categoryID, $manufacturerID, $imgLink));
	}
	public function GetCollectionsNames()
	{
		$records = $this->db->Select('collection', 'name');
		$CollectionsNames = array();
		for ($i=0; $i < count($records); $i++) { 
			array_push($CollectionsNames, $records[$i][0]);
		}
		return $CollectionsNames;
	}
	public function GetCollectionsNamesByCategory($categoryID)
	{
		$records = $this->db->Select('collection', array('id', 'name'), "category=".$categoryID);
		$CollectionsNames = array();
		for ($i=0; $i < count($records); $i++) { 
			array_push($CollectionsNames, array($records[$i][0], $records[$i][1]));
		}
		return $CollectionsNames;
	}
	public function GetCollectionName($id)
	{
		return ($this->db->Select('collection', 'name', "id=".$id))[0][0];
	}
	public function GetCollectionImageLink($id)
	{
		return ($this->db->Select('collection', 'img_link', "id=".$id))[0][0];
	}
	public function GetCollectionInfo($id)
	{
		$records = $this->db->Select('collection', NULL, "id=".$id);
		return $records[0];
	}
	public function GetFullCollectionInfo($id)
	{
		$inf = $this->GetCollectionInfo($id);
		$FullCollectionInfo = array(
			'id' =>  $inf[0],
			'name' => $inf[1],
			'year' => $inf[2],
			'category' => $this->GetCategopryName($inf[3]),
			'manufacturer' => $this->GetManufacturerName($inf[4])
		);
		return $FullCollectionInfo;
	}
	public function GetAllCollections() {
		return $this->db->Select('collection');
	}
	public function RemoveCollection($collID) // warn: if collection contains items - mysql throws error.
	{
		$this->db->Delete('collection', 'id='.$collID);
	}

	//END COLLECTIONS -----------------------------------------------

	// ITEMS --------------------------------------------------------
	public function AddItem($name, $collectionID, $price=0, $imagesIDs='', $colorsIDs='', $userID, $dateTime)
	{
		$lastID = $this->GetLastId('item');
		$this->db->InsertVal('item', array($lastID+1, $name, $collectionID, $price, $imagesIDs, $colorsIDs, $userID, $dateTime));
	}
	public function GetItemInfo($id)
	{
		$info = $this->db->Select('item', NULL, "id=".$id);
		return $info[0];
	}
	public function GetLastItemID()
	{
		return GetLastId('item');
	}
	public function GetItemsByCollection($collectionID)
	{
		$items = $this->db->Select('item', NULL, "collection=".$collectionID);
		return $items;
	}
	public function GetAutorByID($autorID)
	{
		return $this->db->Select('usr', 'name', 'id=\''.$autorID.'\'')[0][0];
	}
	public function RemoveItem($id, $withImages=false)
	{
		// delete images --------------------------------------------
		if($withImages) {
			$imgsArr = explode(',', 
				$this->db->Select('item', 'images', 'id='.$id)[0][0]);
			for($i = 0; $i < count($imgsArr); $i++)
				$this->imgMan->DeleteImage($imgsArr[$i]);
		}
		//-----------------------------------------------------------
		$this->db->Delete('item', 'id='.$id);
	}
	public function EditItem($id, $name, $collectionID, $price=0, $imagesIDs='', $colorsIDs='')
	{
		$fields = $this->db->GetFieldsNames('item');
		array_shift($fields);
		$this->db->Update(	'item',
							$fields,
							array($name, $collectionID, $price, $imagesIDs, $colorsIDs),
							'id='.$id
						);
	}
	public function GetItemsList()
	{
		$items = $this->db->Select('item');
		return $items;
	}
	//END ITEMS -----------------------------------------------------


	//   ***   PRIVATE   ***   --------------------------------------

	private function GetLastId($tableName)
	{
		$records = $this->db->Select($tableName);
		if(count($records) == 0)
			return 0;
		else
			return (int)$records[count($records)-1][0];
		//$this->_DUMP((int)$records[count($records)-1][0]);
	}

	private function _DUMP($value)
	{
		echo '<pre>';
		var_dump($value);
		echo '</pre>';
	}
}

?>
