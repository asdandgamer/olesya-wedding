<?php 
/*	FILE:	DB.php
*	author:	asdandgamer
*	site:	asdandgamer.ucoz.ru
*	e-mail:	asdandgamer@gmail.com
*/

ini_set("display_errors", true);

include_once 'components/DisplayErrors.php';

class DB
{
	private $servername;
	private $username;
	private $password;
	private $dbname;
	private $connection;

//	private $tableName;
	private $isConnected;
	private $mysqlError;
	private $lastRequest;


	// function __construct($servername, $username, $password, $dbname, $tableName)
	// {
	// 	$this->servername = $servername;
	// 	$this->username = $username;
	// 	$this->password = $password;
	// 	$this->dbname = $dbname;
	// 	$this->tableName = $tableName;
	// }

	function __construct()
	{
		$dbSettings = parse_ini_file('.db_settings.ini');
		$this->servername = $dbSettings["DB_HOST"];
		$this->username = $dbSettings["DB_USER"];
		$this->password = $dbSettings["DB_PASS"];
		$this->dbname = $dbSettings["DB_NAME"];
	}

	public function Connect() // Connecting to database
	{
		$this->connection = new mysqli($this->servername, 
			$this->username, $this->password, $this->dbname);
		//---------------------------------------------------
		if ($this->connection->connect_error) {
			$this->mysqlError = $this->connection->connect_error;
			$this->isConnected = false;
		} else {
			$this->mysqlError = NULL;
			$this->isConnected = true;
		}
		// RETURN -------------------------------------------
		return $this->isConnected;
	}
	public function SetCharset($charSet)
	{
		$this->connection->set_charset($charSet);
	}

	private function CheckErrors($requestString, $location, $isEdit=false)
	{
		$res = $this->doRequest($requestString, $isEdit);
		if ($res === false) {
			DisplayMysqlError($this, $location);
			$res = NULL;
		}
		return $res;
	}

	private function doRequest($requestString, $isEdit=false)	// Doing request to database
	{
		if($this->isConnected)
		{
			$arr = array();
			$this->lastRequest = $requestString;
			//------------------------------------------------
			if($isEdit) {
				if ($this->connection->query($requestString) != true) {
					$this->mysqlError = $this->connection->error;
					return false;
				} else {
					$this->mysqlError = NULL;
					return true;
				}
			} else {
				$result = $this->connection->query($requestString);
				//---------------------------------------------
				if($this->connection->error){
					$this->mysqlError = $this->connection->error;
					return false;
				} else {
					$this->mysqlError = NULL;
				}
				//---------------------------------------------
				if ($result->num_rows > 0) {
					while($row = $result->fetch_array(MYSQLI_NUM)) {
						array_push($arr, $row);
					}
				}
			}
			//  RETURN  ---------------------------------------
			return $arr;
		}
	}

	public function Describe($tableName)	// Returns Description of table
	{
		$requestString = "DESCRIBE " . $tableName;
		return $this->doRequest($requestString);
	}

	public function Select($tableName, $fields=NULL, $condition=NULL) // Returns Selected table data by the values and selected columns
	{
		if(is_array($fields))
			$fields = implode(", ", $fields);
		if(is_array($condition))
			$condition = implode(" && ", $condition);
		//---------------------------------------------------------------------
		if($fields != NULL)
			$requestString = "SELECT " . $fields . " FROM " . $tableName;
		else
			$requestString = "SELECT * FROM " . $tableName;
		//---------------------------------------------------------------------
		if($condition != NULL)
			$requestString = $requestString . " WHERE " . $condition;
		//  RETURN  -----------------------------------------------------------
		return $this->CheckErrors($requestString, 
						array(__FILE__, __CLASS__, __FUNCTION__, __LINE__));
	}

	public function Insert($tableName, $fields, $values) // Inserting data to table
	{
		if(is_array($fields))
			$fields = implode(", ", $fields);
		//---------------------------------------------------------------------
		$requestString = "INSERT INTO " . $tableName . " (" . $fields . ") VALUES (";
		$vals = "";
		//---------------------------------------------------------------------
		if(is_array($values)) {
			for($i = 0; $i < count($values); $i++) {
				if(is_numeric($values[$i]))
					$vals = $vals . $values[$i];
				else
					$vals = $vals . '\'' . $values[$i] . '\'';
				if($i<count($values)-1)
					$vals = $vals . ', ';
			}
		} else {
			$vals = $values;
		}
		$requestString = $requestString . $vals . ")";
		//---------------------------------------------------------------------
		return $this->CheckErrors($requestString, 
					array(__FILE__, __CLASS__, __FUNCTION__, __LINE__), true);
	}

	public function Delete($tableName, $condition=NULL) // Removing data from table. If no condition -> table will be empty
	{
		if($condition === NULL)
			$requestString = "DELETE FROM " . $tableName;
		else {
			if(is_array($condition))
				$condition = implode(" && ", $condition);
			//-----------------------------------------------------------------
			$requestString = "DELETE FROM " . $tableName . " WHERE " . $condition;
		}
		//---------------------------------------------------------------------
		$this->CheckErrors($requestString, 
					array(__FILE__, __CLASS__, __FUNCTION__, __LINE__), true);
	}

	public function Update($tableName, $fields, $values, $condition)
	{
		if(is_array($condition))
			$condition = implode(" && ", $condition);
		$requestString = 'UPDATE ' . $tableName . ' SET ';
		$columns = "";
		//---------------------------------------------------------------------
		if(is_array($fields)) {
			for ($i=0; $i < count($fields); $i++) { 
				if(is_numeric($values[$i]))
					$columns = $columns . $fields[$i] . '=' . $values[$i];
				else
					$columns = $columns . $fields[$i] . '=\'' . $values[$i] . '\'';
				if($i<count($values)-1)
					$columns = $columns . ', ';
			}
		} else {
			if(is_numeric($values)){
				$columns = $fields . '=' . $values;
			} else{
				$columns = $fields . '=\'' . $values . '\'';
			}
		}
		//---------------------------------------------------------------------
		$requestString = $requestString . $columns . ' WHERE ' . $condition;
		//--------------------------------------------------------------------- 
		$this->CheckErrors($requestString, 
					array(__FILE__, __CLASS__, __FUNCTION__, __LINE__), true);
	}

	public function InsertVal($tableName, $values)
	{
		$this->Insert($tableName, $this->GetFieldsNames($tableName), $values);
	}

	public function GetFieldsNames($tableName)	// Returns Fields names
	{
		$fieldsNames = array();
		$tableDescription = $this->Describe($tableName);
		for($i = 0; $i < count($tableDescription); $i++) {
			array_push($fieldsNames, $tableDescription[$i][0]);
		}
		return $fieldsNames;
	}

	public function ExecNoResult($string) // Exec MySQL command without results like ALTER TABLE;
	{ 
		return $this->doRequest($string, true);
	}

	// GETTERS ----------------------------------------------------------------
	public function GetLastError()
	{
		return $this->mysqlError;
	}

	public function GetLastRequest()
	{
		return $this->lastRequest;
	}
	//-------------------------------------------------------------------------

	function __destruct()
	{
		if($this->isConnected)
			$this->connection->close();
	}
}

?>