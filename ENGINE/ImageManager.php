<?php /*
file:	ImageManager.php @ Engine
autor:	asdandgamer
e-mail: asdandgamer@gmail.com
create:	23.01.2018
---------------------------------------------------------------
description:	Manage images and thumbnails. Move files & write that to MySQL. Also remove them.
*/
ini_set('display_errors', true);

require_once 'DB.php';
require_once 'ResizeImage.php';

class ImagesManager
{
	private $db;

	function __construct()
	{
		$this->db = new DB();
		$this->db->Connect();
	}

	public function AddImage($file)
	{
		$dbData = array();
		$imgID = $this->GetLastId('images')+1;
		array_push($dbData, $imgID);
		$date = date("Y-m");
		//-----------------------------------------------------------
		$saveFolder = $this->Dir('/IMG/' . $date . '/');
		$originalImage = IResizeImage::ResizeImage($_SERVER["DOCUMENT_ROOT"] . '/IMG/', $file, 1100, ($_SERVER["DOCUMENT_ROOT"] . $saveFolder));
		array_push($dbData, $saveFolder.$originalImage);
		//-----------------------------------------------------------
		$saveFolder = $this->Dir('/IMG/' . $date, '/400x400/');
		$s400 = IResizeImage::ResizeImage($_SERVER["DOCUMENT_ROOT"] . '/IMG/', $file, 400, ($_SERVER["DOCUMENT_ROOT"] . $saveFolder));
		array_push($dbData, $saveFolder.$s400);
		//-----------------------------------------------------------
		$saveFolder = $this->Dir('/IMG/' . $date, '/200x200/');
		$s200 = IResizeImage::ResizeImage($_SERVER["DOCUMENT_ROOT"] . '/IMG/', $file, array(200, 200), ($_SERVER["DOCUMENT_ROOT"] . $saveFolder));
		array_push($dbData, $saveFolder.$s200);
		//-----------------------------------------------------------
		$saveFolder = $this->Dir('/IMG/' . $date, '/40x40/');
		$s40 = IResizeImage::ResizeImage($_SERVER["DOCUMENT_ROOT"] . '/IMG/', $file, array(40, 40), ($_SERVER["DOCUMENT_ROOT"] . $saveFolder));
		array_push($dbData, $saveFolder.$s40);
		//-----------------------------------------------------------
		if($this->db->InsertVal('images', $dbData))
			return $this->db->GetLastError().$this->db->GetLastRequest();

		unlink($_SERVER["DOCUMENT_ROOT"] . '/IMG/' . $file);
		return $imgID;
	}
	public function DeleteImage($imgID)
	{
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . $this->GetOriginal($imgID))) { unlink($_SERVER["DOCUMENT_ROOT"] . $this->GetOriginal($imgID)); }
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . $this->Gets400($imgID))) { unlink($_SERVER["DOCUMENT_ROOT"] . $this->Gets400($imgID)); }
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . $this->Gets200($imgID))) { unlink($_SERVER["DOCUMENT_ROOT"] . $this->Gets200($imgID)); }
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . $this->Gets40($imgID))) { unlink($_SERVER["DOCUMENT_ROOT"] . $this->Gets40($imgID)); }
		$this->db->Delete('images', 'id='.$imgID);
	}
	//---------------------------------------------------------------
	public function GetOriginal($imgID)
	{
		return $this->db->Select('images', 'original', 'id='.$imgID)[0][0];
	}
	public function Gets400($imgID)
	{
		return $this->db->Select('images', 's400', 'id='.$imgID)[0][0];
	}
	public function Gets200($imgID)
	{
		return $this->db->Select('images', 's200', 'id='.$imgID)[0][0];
	}
	public function Gets40($imgID)
	{
		return $this->db->Select('images', 's40', 'id='.$imgID)[0][0];
	}
	//---------------------------------------------------------------
	// private function MoveImage($targetPath, $fileName)
	// {
	// 	rename('image1.jpg', 'del/image1.jpg');
	// }
	private function Dir($dir, $subdir=NULL)
	{
		$_dir = $_SERVER["DOCUMENT_ROOT"] . $dir;
		if(!is_dir($_dir))
			mkdir($_dir);
		if($subdir!=NULL){
			if(!is_dir($_dir . $subdir))
				mkdir($_dir . $subdir);
			return $dir . $subdir;
		} else {
			return $dir;
		}		
	}
	private function GetLastId($tableName)
	{
		$records = $this->db->Select($tableName);
		if(count($records) == 0)
			return 0;
		else
			return (int)$records[count($records)-1][0];
	}
}
?>
