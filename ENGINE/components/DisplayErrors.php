<?php
/*	FILE:	DisplayErrors.php
*	author:	asdandgamer
*	site:	asdandgamer.ucoz.ru
*	e-mail:	asdandgamer@gmail.com
*/

function DisplayMysqlError($db, $location)
{
	$errorSTR = "<h2>" . $location[0] . "</h2></br><b>" . $location[1] . "->" . $location[2] . "()</b>" 
			. "</br><b>Line:</b> " . $location[3] 
			. "</br><b>MySQL error message:</b> \"" . $db->GetLastError() 
			. "\"</br><b>Last request:</b> \"" . $db->GetLastRequest() . "\"</br>";
	print_r($errorSTR);
}

?>
