<?php  /*
* file:		Statistic.php @ ENGINE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		24.02.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once 'DB.php';

class StatisticManager
{
	private $db;

	function __construct()
	{
		$this->db = new DB();
		$this->db->Connect();
	}
	public function TryWrite($ipinfo='', $latitude=NULL, $longitude=NULL)
	{
		$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$browser = $_SERVER['HTTP_USER_AGENT'];
		$ref_link = isset($ipinfo["ref"]) ? $ipinfo["ref"] : 'direct';
		if ($ipinfo=='') {
			$ipinfo = json_decode(shell_exec('curl ipinfo.io/'. $ip));
		} else {
			$ipinfo = (object)$ipinfo;
		}
		$this->Append($ip, $date, $time, $browser, $ref_link, addslashes($ipinfo->city), 
				addslashes($ipinfo->region), addslashes($ipinfo->country),
				addslashes($ipinfo->org), $latitude, $longitude);
	}
	private function Append($ip, $date, $time, $browser, $ref_link, 
							$city, $region, $country, $provider, 
							$latitude, $longitude)
	{
		$data = array(	$ip, $date, $time, $browser, $ref_link, $city,
						$region, $country, 	$provider, $latitude, $longitude);
		$this->db->InsertVal('statistic', $data);
	}
}

?>