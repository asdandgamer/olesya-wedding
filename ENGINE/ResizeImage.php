<?php /*
file:	ResizeImage.php @ Engine
autor:	asdandgamer
e-mail: asdandgamer@gmail.com
create:	19.01.2018
---------------------------------------------------------------
description:	Resizes image file.
Syntax:			ResizeImage('<location>', 
							'<file_name>', 
							array(<target_width(integer)>, <target_height(integer)>), 
							'<save folder>',
							<need proportional resize(bool)>,
							<need height(bool)> // true: size [400 x ?] pixels : proportions by width
												// false: size [? x 400] pixels : proportions by height
						);
Example:		IResizeImage::ResizeImage($_SERVER["DOCUMENT_ROOT"].'/IMG/', 
										'12.jpg', 
										array(400, 400), 
										$_SERVER["DOCUMENT_ROOT"].'/IMG/400x400/');
Return:			[false] if Error;
				[12.jpg_thumb.jpg] if OK;
*/
ini_set('display_errors', true); // !!!!! remove after debug  !!!!!!

class IResizeImage
{
	public static function ResizeImage($path, $file_name, $target_size, $target_path, $proportional=false, $need_height=false)
	{
		$resizeImage = new ResizeImage($path, $file_name, $target_size, $proportional, $need_height);
		$value = $resizeImage->ResizeImage($target_path);
		if(!$value)
			return $resizeImage->ErrMsg();
		return $value;
	}
}


class ResizeImage
{
	private $path;			// path to file
	private $img_name;		// sorce image name
	private $image_type;	// sorce image type
	private $s_width;		// sorce image width
	private $s_height;		// sorce image height
	private $target_width;
	private $target_height;

	private $is_init; // checks is image inited

	private $err_msg=false;

	// Constructor for targeted resizing or by block size.
	// Usage Examples:
	// ResizeImage($_SERVER["DOCUMENT_ROOT"].'/IMG/', '12.jpg', array(400, 400))	: target resizing
	// ResizeImage($_SERVER["DOCUMENT_ROOT"].'/IMG/', '12.jpg', 400)				: proportional resizing to block
	// ResizeImage($_SERVER["DOCUMENT_ROOT"].'/IMG/', '12.jpg', 400, true, true)	: size [400 x ?] pixels : proportions by width
	// ResizeImage($_SERVER["DOCUMENT_ROOT"].'/IMG/', '12.jpg', 400, true, false)	: size [? x 400] pixels : proportions by height
	function __construct($path, $file_name, $target_size, $proportional=false, $need_height=false)
	{
		$this->Init($path, $file_name, $target_size, $proportional, $need_height);
		$this->is_init = true;
	}
	//----------------------------------------------------------------
	private function SetErrMsg($msg)
	{
		$this->err_msg = $msg;
	}

	private function Init($path, $file_name, $target_size, $proportional, $need_height)
	{
		$this->path = $path;
		$this->img_name = $file_name;
		$this->GetImgProperties();
		//-----------------------------------------------
		if(is_array($target_size)) { // targeted resize
			$this->target_width = $target_size[0];
			$this->target_height = $target_size[1];
		} 
		else if(is_integer($target_size) && $target_size > 0) {
			if($proportional &&  $need_height) { 
				$this->GetProportionalHeight($target_size);
			} else if ($proportional &&  (!$need_height)){
				$this->GetProportionalWidth($target_size);
			} else { // proportional resize by block size 
				$this->GetProportionsSize($target_size);
			}
		} else {
			$this->SetErrMsg("Invalid arguments! ResizeImage::__construct($path, $file_name, $target_size)");
			// return;
		}
	}

	private function GetImgProperties() // Gets IMG size & IMG Type()
	{
		$source_properties = getimagesize($this->path . $this->img_name);
		$this->s_width = $source_properties[0];	//source image width
		$this->s_height = $source_properties[1];	//source image height
		$this->image_type = $source_properties[2];
	}

	private function GetProportionalWidth($target_size)
	{
		$x = ($target_size * $this->s_width) / $this->s_height;
		return round($x);
	}

	private function GetProportionalHeight($target_size)
	{
		$x = ($target_size * $this->s_height) / $this->s_width;
		return round($x);
	}

	private function GetProportionsSize($target_size)
	{
		if($this->s_width > $this->s_height) {
			$this->target_width = $target_size;
			$this->target_height = $this->GetProportionalHeight($target_size);
		} else if ($this->s_width < $this->s_height) {
			$this->target_height = $target_size;
			$this->target_width = $this->GetProportionalWidth($target_size);
		} else {
			$this->target_width = $target_size;
			$this->target_height = $target_size;
		}
	}

	// public function ResizeImage($path, $file_name, $target_size, $target_folder, $proportional=false, $need_height=false)
	// {
	// 	$this->Init($path, $file_name, $target_size, $proportional, $need_height);
	// 	$this->is_init = true;
	// 	$this->tResizeImage($target_folder);
	// }

	public function ResizeImage($target_folder)
	{
		if($this->err_msg) {
			return $this->err_msg;
		}
		if (!($this->is_init)) {
			$this->SetErrMsg("Image not initialized, use ! ResizeImage::ResizeImage()");
			return false;
		} else {
			return $this->tResizeImage($target_folder);
		}

	}

	private function tResizeImage($target_folder)
	{
		switch ($this->image_type) {
			case IMAGETYPE_JPEG:
				$image_resource_id = imagecreatefromjpeg($this->path . $this->img_name);
				$target_layer = $this->GetReadyLayer($image_resource_id);
				imagejpeg($target_layer, $target_folder . $this->img_name);
				break;
			case IMAGETYPE_GIF:
				$image_resource_id = imagecreatefromgif($this->path . $this->img_name);
				$target_layer = $this->GetReadyLayer($image_resource_id);
				imagegif($target_layer, $target_folder . $this->img_name);
				break;
			case IMAGETYPE_PNG:
				$image_resource_id = imagecreatefrompng($this->path . $this->img_name);
				$target_layer = $this->GetReadyLayer($image_resource_id);
				imagepng($target_layer, $target_folder . $this->img_name);
				break;
			default:
				return $this->SetErrMsg("Resizing failed! ResizeImage::tResizeImage()");
				// return false;
		}
		return $this->img_name;
	}

	private function GetReadyLayer($image_resource_id) {
		$target_layer=imagecreatetruecolor($this->target_width, $this->target_height);
		imagecopyresampled($target_layer,$image_resource_id,0,0,0,0,
			$this->target_width, $this->target_height, $this->s_width, $this->s_height);
		return $target_layer;
	}

	public function ErrMsg()
	{
		return $this->err_msg;
	}
}

?>