<?php
/*	FILE:	Engine.php
* ________________________________
*
*	author:	asdandgamer
*	site:	asdandgamer.ucoz.ru
*	e-mail:	asdandgamer@gmail.com
* ________________________________
* 
*	DESCRIPTION: Contains all engine files need to be included.
*/

ini_set("display_errors", true);

//include_once 'engine/Site.php';
include_once 'DB.php';
include_once 'ResizeImage.php';
include_once 'ItemsManager.php';
include_once 'ImageManager.php';
include_once 'Site.php';

?>