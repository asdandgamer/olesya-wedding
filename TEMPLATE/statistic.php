<?php /*
* file:		statistic.php @ TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		24.02.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
?>
<!DOCTYPE html>
<html>
<head>
	<title>Real Statistic</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div>
  <input type="button" name="" onclick="SendStatisticDataToServer()" value="SEND">
  <!-- <input type="button" name="" onclick="SendStatisticDataToServer()" value="GETIPINFO"> -->
</div>
<div class="err_msg"></div>
<script type="text/javascript">
/*
	var options = {
		enableHighAccuracy: true,
		timeout: 5000,
		maximumAge: 0
	};

	function success(pos) {
	  var crd = pos.coords;
	  console.log('Your current position is:');
	  console.log(`Latitude : ${crd.latitude}`);
	  console.log(`Longitude: ${crd.longitude}`);
	  console.log(`More or less ${crd.accuracy} meters.`);
	};

	function error(err) {
	  console.warn(`ERROR(${err.code}): ${err.message}`);
	};

	navigator.geolocation.getCurrentPosition(success, error, options);
*/

function SendStatisticDataToServer() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(Position);
	} else {
		IpInfo(null);
	}
}
function Position(position) {
	var geoPosition = [position.coords.latitude, position.coords.longitude];
	IpInfo(geoPosition);
}
function IpInfo(geoPosition) {
	$.ajax({
		url: "https://ipinfo.io/json",
		type: "GET",
		dataType: "json",
		success: function(response) {
			SendStatisticData(response, geoPosition);
		},
		error: function(xhr) {
			console.error(xhr);
			SendStatisticData(null, geoPosition);
		}
		
	});

}
//{"ip":"194.44.135.39","city":"Ternopil","region":"Ternopil's'ka Oblast'","country":"UA","loc":"49.5559,25.6056","org":"AS3255 State"}
function SendStatisticData(ipinfo, geoPosition) {
	var Data; 
	if (ipinfo == null && Array.isArray(geoPosition)) {
		Data = {
			info: 1,
			latitude: geoPosition[0],
			longitude: geoPosition[1]
		}
	} else if(ipinfo == null && !(Array.isArray(geoPosition))) {
		Data = {
			info: 3
		}
	} else if(Array.isArray(geoPosition)) {
		Data = {
			info: 1,
			city: ipinfo.city,
			region: ipinfo.region,
			country: ipinfo.country,
			latitude: geoPosition[0],
			longitude: geoPosition[1]
		}
	} else {
		Data = {
			info: 2,
			city: ipinfo.city,
			region: ipinfo.region,
			country: ipinfo.country,
			loc: ipinfo.loc
		}
	}
	// $city, $region, $country, $provider,  $latitude, $longitude
	$.ajax({
		type: "POST",
		url: 'AJAX/Statistic.php',
		data: Data,
		dataType: "json",
		success: function(data) {
			alert(JSON.stringify(data));
		},
		error: function(xhr) {
			console.error(xhr);
			$('.err_msg').html(xhr.responseText);
		}
	});
}

</script>
</body>
</html>


