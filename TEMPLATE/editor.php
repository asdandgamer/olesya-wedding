<!DOCTYPE html>
<html>
<head>
	<title>Olesya Wedding Editor</title>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow">
	<link rel="stylesheet" type="text/css" href="CSS/editor.css">
	<script type="text/javascript" src="./JS/jquery-3.2.1.min.js"></script>
	<script src="JS/editor.js"></script>
	<!-- <script type="text/javascript">
		function ShowVisitors() {
			$('.editor_content').css('display', 'none');
			$('.visitsinfo').css('display', 'table-cell');
		}
		function ShowEditor() {
			$('.visitsinfo').css('display', 'none');
			$('.editor_content').css('display', 'table-cell');
		}
		function GoToMain() {
			$(location).attr('href', '/');
		}
	</script> -->
</head>
<body>
	<div class="wrap" style="display: table;">
		<?php	require_once 'editor_menu.php';
				require_once 'visits.php';
		?>
	<!-- 
		<div class="left_menu"></div> -->
		<div class="editor_content">
			<div class="header"><h1>Редактор</h1></div>
			<form class="item_description">
				<div class="std_field"><label class="std_size std_label">Назва: </label><input id="item_name" class="std_size std_edit" type="text" name=""></div>
				<div class="std_field"><label class="std_size std_label">Ціна: </label><input id="item_price" class="std_size std_edit" type="text" name=""></div>
				<div class="std_field">
					<label class="std_size std_label">Категорія: </label>
					<select class="std_size std_select item_select" id="categories" onchange="LoadCollections()"></select>
					<!-- <button id="add_category" onclick="">Add Category</button> -->
				</div>
				<div class="std_field">
					<label class="std_size std_label">Назва колекції: </label>
					<select class="std_size std_select item_select" id="collections" onchange="LoadCollectionInfo()"></select>
					<input type="button" class="std_button" name="" onclick="DisplayCollectionEditor()" value="Додати Колекцію">
					<!-- <button id="add_collection" onclick="DisplayCollectionEditor()">Add Collection</button> -->
				</div>
				<div class="std_field"><label class="std_size std_label">Виробник: </label><span id="man_sp">...</span></div>
				<div class="std_field"><label class="std_size std_label">Рік: </label><span id="year_sp">...</span></div>
			</form>
			<!-- <div class="colors_container">
				<h3>Colors:</h3>
				<div class="colors" style="display: inline-block;"></div>
				<div class="new_img" onclick="DisplayInstruments()"></div>
			</div> -->
			<div class="images_container">
				<h3>Зображення: </h3>
				<div class="images" style="display: inline-block;"></div>
				<div class="new_img" onclick="DisplayInstruments()"></div>
			</div>
			<div class="std_button_container">
				<input class="std_button ok_btn" type="button" name="AddItem" onclick="AddItem()" value="Опублікувати">
			</div>
		</div>
	</div>
	<div class="owerdraw">
		<div class="std_editor_form add_img_form">
			<div class="img_upload_container">
				<form id="Form" action="" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input class="file_input" type="file" id="myfile" />
						<div class="progress">
							<div class="myprogress" role="progressbar">0%</div>
						</div>
						<input class="std_button" type="button" id="btn" onclick="Upload()" value="Upload" />
					</div>
				</form>
				<div class="owerdraw_prew_img"><img id="load_img" src="IMG/load.gif" width="200" height="200" style="display: none;"></div>
			</div>
			<div class="std_button_container">
				<button id="btn_add_image" class="std_button ok_btn">Додати</button>
				<button id="btn_close" class="std_button std_btn_right cancel_btn" style="float: right;">Закрити</button>
			</div>
		</div>
	</div>
	<div class="collection_editor">
		<form class="std_editor_form collection_editor_form">
			<div class="std_field"><label class="std_size std_label">ID: </label><span class="std_size" id="collection_id">...</span></div>
			<div class="std_field"><label class="std_size std_label">Назва: </label><input id="coll_name" class="std_size std_edit" type="text" name="coolection_name"></div>
			<div class="std_field"><label class="std_size std_label">Рік: </label><input id="coll_year" class="std_size std_edit" type="number" name="collection_year"></div>
			<div class="std_field"><label class="std_size std_label">Виробник: </label><select class="std_size std_select" id="manuf_sel"></select></div>
			<div class="std_field"><label class="std_size std_label">Категорія: </label><select class="std_size std_select" id="catg_sel"></select></div>
			<div class="std_field"><label class="std_size std_label">Зображення(шлях): </label><input id="coll_prev_image" class="std_size std_edit" type="text"></div>
			<div class="std_button_container">
				<input type="button" class="std_button ok_btn" name="" value="Додати" onclick="AddCollection()">
				<input type="button" class="std_button std_btn_right cancel_btn" name="" value="Закрити" onclick="CloseCollectionEditor()">
			</div>
		</form>
	</div>
</body>
</html>