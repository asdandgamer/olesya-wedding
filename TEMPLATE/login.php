<!DOCTYPE html>
<html>
<head>
	<title>Login Form</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/editor.css">
	<script type="text/javascript" src="JS/jquery-3.2.1.min.js"></script>
	<script type="text/javascript">
		function LogIn() {
			data = {
				login: $('#login').val(),
				password: $('#passwd').val()
			};
			$.post('login.php', data).done(function(data) {
				if(data==true) { // All fine :-)
					$('#login_err').hide();
					GoTo();
				} else { // Wrong user name or passwd!
					$('#login_err').fadeIn();
					$('#login_err').delay(3000).fadeOut();
				}
			});
		}
		function LogOut() {
			$.get('login.php', { data: 'LogOut'}).done(function(data) {
				if(data) {
					GoTo();
				}
			});
		}
		function GoTo() {
			var get = location.search;
			if(get == '?location=admt')
				window.location.href = "/ADMT/";
			else if(get == '?location=admt_users')
				window.location.href = "/ADMT/users.php";
			else
				window.location.href = "/ADMT/";
		}
		function getCookie(cname) {
		    var name = cname + "=";
		    var ca = document.cookie.split(';');
		    for(var i=0; i<ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1);
		        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
		    }
		    return "";
		}
	</script>
	<style type="text/css">
		.loginForm {
			margin-top: 50px;
			display: block; 
			width: 652px; 
			margin: 0 auto;
		}
	</style>

</head>
<body>
<div class="wrap">
	<div class="loginForm">
		<div class="std_editor_form">
			<div id="login_err" style="display: none; border-left: 3px solid red; width: inhterit; margin: 4px 2px; font-size: 18px; font-family: 'Arial'; line-height: 32px; box-shadow: 0px 0px 4px grey; padding-left: 5px; border-radius: 2px;">Помилка: Ім'я користувача або пароль невірні!</div>
			<?php
				if (Site::getLogInStatus()) {
					echo '<div class="std_size" style="width: 100%">SSID: ' . session_id();
					echo "<br>ID:" . Site::getUserId() . " | Логін: " . Site::getUserName() . "<br>";
					echo '<div class="std_button_container coll_btn_container">';
					echo '<input class="std_button std_btn_right cancel_btn" id="btn_logout" type="button" name="LogOut" value="Вийти" onclick="LogOut()">';
					echo '<input class="std_button" style="margin-left: 5px;" type="button" value="Перейти у Редактор" onclick="GoTo()"></div>';
					echo '</div>';
					echo '<hr style="margin-top: 0; margin-bottom: 5px; border-top: 1px solid #555; ">';
				}
			?>
			<form class="loginform">
				<div class="std_field">
					<label class="std_size std_label">Логін:</label><input id="login" class="std_size std_edit" type="text" name="login" />
				</div>
				<div class="std_field">
					<label class="std_size std_label">Пароль:</label><input id="passwd" class="std_size std_edit" type="password" name="password" />
				</div>
				<div class="std_button_container coll_btn_container">
					<input class="std_button ok_btn" type="button" value="Увійти" name="log_in" onclick="LogIn()" />
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>