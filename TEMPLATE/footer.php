<?php 
require_once 'ENGINE/Engine.php';
  $db = new DB();
  $db->Connect();
  function DrawFMenu($categoryID)
  {
    $itemsManager = new ItemsManager();
    $list = $itemsManager->GetCollectionsNamesByCategory($categoryID);
      for ($i=0; $i < count($list); $i++) { 
        echo '<li><a href="collection.php?CollID=' . $list[$i][0] . '">'
      . $list[$i][1] . '</a></li>';
    }
  }
  $all_visits = $db->Select('visitsinfo', 'COUNT(1)')[0][0];
  $visits_per_day = count($db->Select('visitsinfo', 'day', 'day=\''.date("Y-m-d").'\''));
  $visits_per_yesterday = count($db->Select('visitsinfo', 'day', 'day=SUBDATE(CURDATE(),1)'));
?>
<!-- scripts -->
  <script async="async" type="text/javascript" src="./JS/main.js"></script>
<!-- end scripts -->
  <div class="footer">
    <div class="top_footer">
      <div class="contacts_at_foot">
        <span style="color: #dfc154; font-weight: bold;">Тернопіль, бульв. Шевченка, 12<br>
        (ЦУМ 2 поверх, ліве крило)<br> </span>
        <span>+380975207184, +380974336853</span>
      </div>
      <div class="socials socials_at_foot">
        <div>Ми в соцмережах</div>
        <a class="social_icon" href="https://fb.com"><div><img src="./img/fb.png"></div></a>
        <a class="social_icon" href="https://plus.google.com/u/0/114700116214496142102"><div><img src="./img/gplus.png"></div></a>
        <a class="social_icon" href="https://www.instagram.com/olesya_salon_ternopil/"><div><img src="./img/inst.png"></div></a>
      </div>
    </div>
    <div class="partners" style="display: table; width: 100%; margin-bottom: 10px; border-top: 1px solid grey; padding-top: 10px;">
      <div class="partners_label" style="padding-left: 50px; font-family: Arial; font-weight: bold; color: #dfc154;">Наші партнери</div>
      <div class="partner" style="width: 220px">
        <a href="http://zevs-mebli.com.ua">
          <div class="partner_label" style="text-align: center; color: grey; font-family: Arial; font-weight: bold; margin: 4px 0px;">ZEVS Mebli</div>
          <div class="partner_logo">
            <img src="img/zevs1.png" width="200" height="200"></img>
          </div>
        </a>
      </div>
    </div>
    <div class="bottom_footer" style="border-top: 1px solid grey; padding-top: 10px;">
      <div class="bottom_footer_category"><div>ВЕСІЛЬНІ</div>
        <ul id="BC1"><?php DrawFMenu(1); ?></ul>
      </div>
      <div class="bottom_footer_category"><div>ВЕЧІРНІ</div>
        <ul id="BC2"><?php DrawFMenu(2); ?></ul>
      </div>
      <div class="bottom_footer_category"><div>ДИТЯЧІ</div>
        <ul id="BC3"><?php DrawFMenu(3); ?></ul>
      </div>
      <div class="bottom_footer_category"><div>АКСЕСУАРИ</div>
        <ul>
          <li><a href="category.php?CatID=11">БІЖУТЕРІЯ</a></li>
          <li><a href="category.php?CatID=12">ПОЯС</a></li>
          <li><a href="category.php?CatID=13">ФАТА</a></li>
          <li><a href="category.php?CatID=14">ШУБКА</a></li>
        </ul>
      </div>
      <div class="bottom_footer_category"><div>КРАСУНІ</div>
        <ul>
          <li><a href="category.php?CatID=21">Весільні</a></li>
          <li><a href="category.php?CatID=22">Вечірні</a></li>
          <li><a href="category.php?CatID=23">Дитячі</a></li>
        </ul>
      </div>
    </div>
    <div style="width: 100%; padding-bottom: 10px; display: table; font-family: 'Goudy Bookletter 1911', sans-serif; color: grey;">
      <div style="display: table-cell;">Перегляди сьогодні: <?php echo $visits_per_day; ?></div>
      <div style="display: table-cell;">Перегляди вчора: <?php echo $visits_per_yesterday; ?></div>
      <div style="display: table-cell;">За весь час: 5<?php echo $all_visits; ?></div>
    </div>
  </div>
</div>
</body>
</html>