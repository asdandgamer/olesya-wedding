<?php /*
* file:   item.php @ TEMPLATE
* autor:  asdandgamer
* e-mail: asdandgamer@gmail.com
* date:   16.02.2018
*/
?>
<div class="breadcrumbs">
  <a href="/"><img src="img/home.png"></a> &gt;
  <a href="/category.php?CatID=<?php echo $categoryInf[0]; ?>">
    <?php echo $categoryInf[1];?>
  </a> &gt; 
  <a href="/collection.php?CollID=<?php echo $collectionInf[0]; ?>">
    <?php echo $collectionInf[1]; ?>
  </a> &gt; 
  <?php echo $item[1]; ?>
</div>
<div class="content_wrap">
  <div class="product_categories">
    <div class="text"><?php echo $item[1]; ?></div>
    <div class="image_slide_buttons_container">
    	<div id="prev_btn" class="nav_btn"></div>
    	<div id="next_btn" class="nav_btn"></div>
    </div>
    <div id="main_img" style="position: relative; z-index: 1;"><img src=""></div>
    <hr style="margin-top: 5px; margin-bottom: 3px;">
    <div id="slider_div">
      <!-- <div id="prev_btn" class="nav_btn"></div> -->
      <div style="display: block;  margin-left: auto; margin-right: auto;">
  	    <ul id="slider"></ul>
      </div>
      <!-- <div id="next_btn" class="nav_btn"></div> -->
    </div>
  </div>
</div>
<script id="data" type="application/json"><?php echo json_encode(explode(", ",$item[4])); ?></script>
<script src="JS/SlideImg.js"></script>