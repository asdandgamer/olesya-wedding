<?php /*
* file:		suggestions.php @ TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		27.02.2018
*/ 
?>
<div class="content_wrap">
  <div class="breadcrumbs">
  	<a href="/"><img src="img/home.png"></a>
  	 &gt; <?php echo $categoryName; ?>
  </div>
  <div class="product_categories">
    <div class="text"><?php echo $categoryName; ?></div>
    <a href="/item.php?itemID=200">
      <div id="200" class="product_category"> 
        <div class="label">
          <span>Annabella</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="548" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/3 Annabella (1) .jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <a href="/item.php?itemID=132">
      <div id="132" class="product_category"> 
        <div class="label">
          <span>940</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="323" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/940(1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <a href="/item.php?itemID=442">
      <div id="442" class="product_category"> 
        <div class="label">
          <span>Lilith</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="1230" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/22 Lilith (1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <!--  -->
    <a href="/item.php?itemID=17">
      <div id="17" class="product_category"> 
        <div class="label">
        	<span>1316</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="38" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/1316(1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <a href="/item.php?itemID=371">
      <div id="371" class="product_category">
        <div class="label">
        	<span>V-148</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="977" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/V-148 (1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <a href="/item.php?itemID=392">
      <div id="392" class="product_category">
        <div class="label">
        	<span>VN-012</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="1067" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/VN-012 (1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <!--  -->
    <a href="/item.php?itemID=3">
      <div id="3" class="product_category"> 
        <div class="label">
          <span>1302</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="8" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/1302(1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <a href="/item.php?itemID=397">
      <div id="397" class="product_category"> 
        <div class="label">
          <span>VN-017</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="1087" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/VN-017 (1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
    <a href="/item.php?itemID=263">
      <div id="263" class="product_category"> 
        <div class="label">
        	<span>1153</span>
        </div>
        <span class="promo_sticker">Акція 
        	-25%</span>
        <div id="700" class="image" style="width: 341px; height: 341px;">
          <img src="IMG/2018-02/400x400/1153(1).jpg" style="width: 341px; height: 341px;">
        </div>
      </div>
    </a>
  </div>
</div>