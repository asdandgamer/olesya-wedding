<?php 
require_once "banned.php";

$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
if(bannedIP($ip)) {
  Site::Redirect('/banned.php');
}

?>

<!DOCTYPE html>
<html>
<head>
  <title><?php echo $Title; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=0.7">
  <?php if (isset($description)) { echo '<meta name="description" content="' . $description . '">'; } ?>
  <?php if (isset($OG)) {
    echo '<meta property="og:type" content="website">'.PHP_EOL;
    echo '<meta property="og:title" content="'. $OG['title'] .'">'.PHP_EOL;
    echo '<meta property="og:url" content="'. $OG['url'] .'">'.PHP_EOL;
    echo '<meta property="og:description" content="'. $OG['description'] .'">'.PHP_EOL;
    echo '<meta property="og:image" content="'. $OG['image'] .'">'.PHP_EOL;
    echo '<meta property="og:site_name" content="Весільний салон ОЛЕСЯ">'.PHP_EOL;
  } ?>
  <!-- <link rel="icon" href="./img/favicon_32.png" sizes="32x32"> -->
  <link rel="icon" href="./img/favicon_192_1.png" sizes="192x192">
  <link rel="stylesheet" type="text/css" href="./CSS/main.css">
  <link rel="stylesheet" type="text/css" href="./CSS/menu.css">
  <link rel="stylesheet" type="text/css" href="./CSS/slider.css">
  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
  <script type="text/javascript" src="./JS/jquery-3.2.1.min.js"></script>
  <!-- <script async="async" type="text/javascript" src="./JS/statistic.js"></script> -->
    <!-- Global site tag (gtag.js)l - Google Anaytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114040826-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-114040826-1');
  </script>
</head>
<body>
<div class="wrap">
  <div class="header">
  	<div class="header_side header_left">
  	  <div class="head_side_menu">
  	    <a href="/about.php"><div>ПРО САЛОН</div></a>
  	    <a href="/suggestions.php"><div>ПРОПОЗИЦІЇ</div></a>
  	  </div>
  	  <div class="contacts_at_head">
  		  <span style="color: #dfc154; font-weight: bold;">Тернопіль, бульв. Шевченка, 12<br>
        (ЦУМ 2 поверх, ліве крило)<br> </span>
      	<span>+380975207184, +380974336853</span><br>
      	Пн.-Пт.: 10:00-19:00<br>
      	Сб.,Нд.: 10:00-18:00<br>
  	  </div>
  	</div>
  	<div class="header_center">
  		<!-- <img src="./img/logo.png"> -->
  	</div>
  	<div class="header_side header_right">
  		<div class="head_side_menu">
  			<a href="video.php"><div>ВІДЕО</div></a>
  			<a href="contacts.php"><div>КОНТАКТИ</div></a>
  		</div>
  		<div class="socials socials_at_head">
  			<div>Ми в соцмережах</div>
  			<a class="social_icon" href="https://fb.com"><div><img src="./img/fb.png"></div></a>
        <a class="social_icon" href="https://plus.google.com/u/0/114700116214496142102"><div><img src="./img/gplus.png"></div></a>
        <a class="social_icon" href="https://www.instagram.com/olesya_salon_ternopil/"><div><img src="./img/inst.png"></div></a>
  		</div>
  	</div>
  </div>
