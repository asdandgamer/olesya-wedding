<div class="content_wrap">
  <div class="breadcrumbs"><a href="/"><img src="img/home.png"></a> &gt; Наші красуні</div>
  <div class="product_categories">
    <div class="text">Наші красуні</div>
    <a href="/category.php?CatID=21">
      <div class="product_category">
        <div class="label">
          <span>Весільні</span>
        </div>
        <div class="image">
          <img src="./IMG/COLLIMG/ateler.jpg">
        </div>
      </div>
    </a>
    <a href="/category.php?CatID=22">
      <div class="product_category">
        <div class="label">
          <span>Вечірні</span>
        </div>
        <div class="image">
          <img src="./IMG/COLLIMG/Siliana-evening.jpg">
        </div>
      </div>
    </a>
    <a href="/category.php?CatID=23">
      <div class="product_category">
        <div class="label">
          <span>Дитячі</span>
        </div>
        <div class="image">
          <img src="./IMG/COLLIMG/Lilia-Child.jpg">
        </div>
      </div>
    </a>
  </div>
</div>