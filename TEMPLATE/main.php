  <h1 style="display: none;">Салон весільного та вечірнього вбрання OLESYA в тернополі.</h1>
  <div class="content_wrap">
    <div class="slider">
      <div class='nav'></div>
      <ul>
        <li><img src="img/slider/1.jpg" alt=""></li>
        <li><img src="img/slider/2.jpg" alt=""></li>
        <li><img src="img/slider/3.jpg" alt=""></li>
        <li><img src="img/slider/4.jpg" alt=""></li>
        <li><img src="img/slider/5.jpg" alt=""></li>
        <li><img src="img/slider/6.jpg" alt=""></li>
        <!-- <li><img src="img/slider/7.jpg" alt=""></li>
        <li><img src="img/slider/8.jpg" alt=""></li> -->
      </ul>
    </div>
    <div class="main_page_labels">Відео</div>
    <div class="video">
      <iframe class="videoFrame" frameborder="0"
        src="https://www.youtube.com/embed/KJQ6VqGTKiM">
      </iframe>
    </div>
    <div class="main_page_labels">Сукні</div>
    <div class="product_categories">
      <a href="/category.php?CatID=1">
        <div class="product_category">
          <div class="label">
            <span>Весільні</span>
          </div>
          <div class="image">
            <img src="./IMG/COLLIMG/ateler.jpg">
          </div>
        </div>
      </a>
      <a href="/category.php?CatID=2">
        <div class="product_category">
          <div class="label">
            <span>Вечірні</span>
          </div>
          <div class="image">
            <img src="./IMG/COLLIMG/Siliana-evening.jpg">
          </div>
        </div>
      </a>
      <a href="/category.php?CatID=3">
        <div class="product_category">
          <div class="label">
            <span>Дитячі</span>
          </div>
          <div class="image">
            <img src="./IMG/COLLIMG/Lilia-Child.jpg">
          </div>
        </div>
      </a>
    </div>
    <div class="main_page_labels">Аксесуари</div>
    <div class="product_categories">
      <a href="/category.php?CatID=14">
        <div class="product_category">
          <div class="label">
            <span>Шубка</span>
          </div>
          <div class="image">
            <img src="IMG/COLLIMG/bolero.jpg">
          </div>
        </div>
      </a>
      <a href="/category.php?CatID=13">
        <div class="product_category">
          <div class="label">
            <span>Фата</span>
          </div>
          <div class="image">
            <img src="IMG/COLLIMG/Siliana-veil-2018.jpg">
          </div>
        </div>
      </a>
      <a href="/category.php?CatID=11">
        <div class="product_category">
          <div class="label">
            <span>Біжутерія</span>
          </div>
          <div class="image">
            <img src="IMG/COLLIMG/bijut.jpg">
          </div>
        </div>
      </a>
    </div>
    <div class="main_page_labels">Знайдіть нас тут</div>
    <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5176.842205192378!2d25.58628027863994!3d49.55207714929291!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd13aef572b8e2aab!2z0JLQtdGB0ZbQu9GM0L3QuNC5INGB0LDQu9C-0L0gLSDQntCb0JXQodCv!5e0!3m2!1suk!2sua!4v1519212389469" width="1080" height="600" frameborder="0" style="border:0" allowfullscreen></iframe></div>
   <!--  <div style="background-color: #222; height: 2px"></div> -->
    <div class="welcome_text">Все для Вас нашi нареченi!</div>
    <div style="height: 24px; width: initial; overflow: hidden; font-size: 14pt; font-family: Arial; color: #929292; padding-left: 20px;">
      Весільні салони   Весільні плаття   Весільні сукні   Весільний салон   Весільна сукня 
      У весільному салоні Олеся Olesya Wedding в Тернополі в ЦУМ'і можна придбати весільні сукні, весільні плаття, весільні аксесуари, такі як: фати, мереживо, пояси, шубки, болеро. Також в наявності є Аксесуари для дружок. Все весільне вбрання - це гарні брендові ексклюзивні плаття і сукні для нареченої.
      Також у нас можете знайти вечірні сукні, випускні плаття для випускниць, бальні сукні. Можна купити у нас хороша красиві гарні брендові вечірна плаття, випускні сукні, ексклюзивні брендові бальні плаття.
      Весь представлений у нас асортимент для молодят за доступними цінами можрна придбати у центрі тернополя.
      Розкішшні сукні у тернополі за низькими цінами. 
      У весільному салоні "ОЛЕСЯ" на Шевченка, 12 ви знайдете весільні, випускні та вечірні сукні від іменитих дизайнерів. Творіння для урочистостей від цих та інших  відомих марок чекають Вас в нашому салоні.
      Весільний салон «Олеся» пропонує широкий асортимент ексклюзивного весільного та вечірнього вбрання провідних виробників за хорошими цінами.
      В асортименті салону — традиційно довгі наряди з пишною, повітряною спідницею, і елегантні, приталені моделі лаконічного дизайну. Вони відмінно підходять для весілля в сучасному стилі. Довгий мереживний рукав або відкриті плечі, вишивка бісером або стрази, асиметрія в крої та аристократичний шлейф — такі деталі дизайну роблять сукні від «Олеся» неповторними, цікавими.
    </div>
    
    <!-- <video controlslist="nodownload" style="width: 674px; height: 379px;" src="https://www.youtube.com/embed/TVvoSdXptbw"></video> -->
  </div>
  <script type="text/javascript" src="./JS/slider.js"></script>