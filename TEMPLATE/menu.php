<?php 
require_once 'ENGINE/Engine.php';
  function DrawMenu($categoryID)
  {
    $itemsManager = new ItemsManager();
    $list = $itemsManager->GetCollectionsNamesByCategory($categoryID);
      for ($i=0; $i < count($list); $i++) { 
        echo '<li><a href="collection.php?CollID=' . $list[$i][0] . '">'
      . $list[$i][1] . '</a></li>';
    }
  }
?>
  <div class="main_menu">
    <!-- ВЕСІЛЬНІ ВЕЧІРНІ ДИТЯЧІ  АКСЕСУАРИ КЛІЄНТИ -->
    <ul class="main_menu_mul">
      <div class="main_menu_empty_space"></div>
      <li><a href="/category.php?CatID=1">ВЕСІЛЬНІ</a>
        <ul id="C1" class="menu_drop"><?php DrawMenu(1); ?></ul>
      </li>
      <li><a href="/category.php?CatID=2">ВЕЧІРНІ</a>
        <ul id="C2" class="menu_drop"><?php DrawMenu(2); ?></ul>
      </li>
      <li><a href="/category.php?CatID=3">ДИТЯЧІ</a>
        <ul id="C3" class="menu_drop"><?php DrawMenu(3); ?></ul>
      </li>
      <li><a href="/accesoires.php">АКСЕСУАРИ</a>
        <ul class="menu_drop">
          <li><a href="category.php?CatID=11">БІЖУТЕРІЯ</a></li>
          <li><a href="category.php?CatID=12">ПОЯС</a></li>
          <li><a href="category.php?CatID=13">ФАТА</a></li>
          <li><a href="category.php?CatID=14">ШУБКА</a></li>
        </ul>
      </li>
      <li><a href="/ladies.php">НАШІ КРАСУНІ</a>
        <ul class="menu_drop">
          <li><a href="category.php?CatID=21">Весільні</a></li>
          <li><a href="category.php?CatID=22">Вечірні</a></li>
          <li><a href="category.php?CatID=23">Дитячі</a></li>
        </ul>
      </li>
      <div class="main_menu_empty_space"></div>
    </ul>
  </div>