<?php /*
* file:		editor_menu.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.02.2018
*/

?>

<div class="left_menu">
	<div class="menu_item" onclick="GoToMain()">
		<div class="menu_item_logo"><img src="img/editor/go_main.png"></div>
		<div class="menu_item_label">Go to main</div>
	</div>
	<div class="menu_item" onclick="ShowVisitors()">
		<div class="menu_item_logo"><img src="img/editor/visitors.png"></div>
		<div class="menu_item_label">visitors</div>
	</div>
	<div class="menu_item" onclick="ShowEditor()">
		<div class="menu_item_logo"><img src="img/editor/editor.png"></div>
		<div class="menu_item_label">Editor</div>
	</div>
	<hr>
	<div class="menu_item" onclick="GoToADMT()">
		<div class="menu_item_logo"><img src="img/editor/editor.png"></div>
		<div class="menu_item_label">ADMT</div>
	</div>
</div>