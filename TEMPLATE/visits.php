<?php /*
* file:		visits.php @ TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		22.02.2018
*/
	$db = new DB();
	$db->Connect();
	echo '<div class="visitsinfo" style="display: none;">';
	$list = '';
?>
<div>
	<a href="/Edit.php?day=all"><input type="button" value="ALL"></a>
	<a href="/Edit.php?day=today"><input type="button" value="Today"></a>
	<a href="/Edit.php?day=yesterday"><input type="button" value="Yesterday"></a>
	<a href="/Edit.php?day=thismonth"><input type="button" value="This month"></a>
</div>
<hr>
<?php
	if (isset($_GET["day"])) {
		switch ($_GET["day"]) {
			case 'today':
				$list = $db->Select('visitsinfo', '*', 'day=\''.date('Y-m-d').'\'');
				break;

			case 'yesterday':
				$list = $db->Select('visitsinfo', '*', 'day=SUBDATE(CURDATE(),1)');
				break;

			case 'thismonth':
				$list = $db->Select('visitsinfo', '*', 'MONTH(`day`) = MONTH(NOW()) AND YEAR(`day`) = YEAR(NOW())');
				break;

			case 'all':
				$list = $db->Select('visitsinfo');
				break;
			
			default:
				# code...
				break;
		}
	} else {
		$list = $db->Select('visitsinfo', '*', 'day=\''.date('Y-m-d').'\'');
		//$list = $db->Select('visitsinfo');
	}
	
	
	echo '<div><table class="visits_table">' . PHP_EOL;
	for ($i=0; $i < count($list); $i++) { 
		echo '<tr>' . PHP_EOL;
		echo '<td width="40" rowspan="4">' . ($i+1) . '</td>';
		echo '<td width="220">';
		echo $list[$i][0]; // at
		echo '</td>';
		//------------------
		echo '<td width="140" class="td_ip">';
		echo $list[$i][4]; // ip
		echo '</td>';
		//-----------------------
		echo '<td width="100" class="spec_td">';
		echo $list[$i][5]; // date
		echo '</td>';
		//-----------------------
		echo '<tr>' . PHP_EOL;
			echo '<td colspan="3" class="ref_link spec_td">';
			echo $list[$i][3]; // ref_link
			echo '</td>';
		echo '</tr>' . PHP_EOL;
		//-----------------------
		echo '<tr>' . PHP_EOL;
			echo '<td colspan="3" class="browser spec_td">';
			echo $list[$i][1]; // browser
			echo '</td>';
		echo '</tr>' . PHP_EOL;
		echo '<tr style="border-bottom: 1px solid black; word-wrap:break-word;">' . PHP_EOL;
			//-----------------------
			echo '<td colspan="3" class="ipinfo spec_td">';
			echo $list[$i][2]; // ipinfo
			echo '</td>';
		echo '</tr>' . PHP_EOL;
		//-----------------------
		echo '</tr>' . PHP_EOL;
	}
	echo '</table></div>' . PHP_EOL;
	// echo '<pre>';
	// print_r($list);
	// echo '</pre>';
	echo '</div>';
?>