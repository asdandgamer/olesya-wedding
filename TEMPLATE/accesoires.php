<?php /*
* file:		accesoires.php @ TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		23.02.2018
*/
?>

<div class="content_wrap">
  <div class="breadcrumbs"><a href="/"><img src="img/home.png"></a> &gt; <?php echo $categoryName; ?></div>
  <div class="product_categories">
    <div class="text"><?php echo $categoryName; ?></div>
    <a href="/category.php?CatID=14">
      <div class="product_category">
        <div class="label">
          <span>Шубка</span>
        </div>
        <div class="image">
          <img src="IMG/COLLIMG/bolero.jpg">
        </div>
      </div>
    </a>
    <a href="/category.php?CatID=13">
      <div class="product_category">
        <div class="label">
          <span>Фата</span>
        </div>
        <div class="image">
          <img src="IMG/COLLIMG/Siliana-veil-2018.jpg">
        </div>
      </div>
    </a>
    <a href="/category.php?CatID=11">
      <div class="product_category">
        <div class="label">
          <span>Біжутерія</span>
        </div>
        <div class="image">
          <img src="IMG/COLLIMG/bijut.jpg">
        </div>
      </div>
    </a>
    <a href="/category.php?CatID=12" style="display: none;">
      <div class="product_category">
        <div class="label">
          <span>Пояс</span>
        </div>
        <div class="image">
          <img src="">
        </div>
      </div>
    </a>
  </div>
</div>