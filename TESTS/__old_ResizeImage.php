function ResizeImage($path, $file, $width, $height)
{
	$source_properties = getimagesize($path . $file);
	$image_type = $source_properties[2];
	$s_width = $source_properties[0];	//source image width
	$s_height = $source_properties[1];	//source image height
	switch ($image_type) {
		case IMAGETYPE_JPEG:
			$image_resource_id = imagecreatefromjpeg($path . $file);
			$target_layer = fn_resize($image_resource_id, $width, $height, $s_width, $s_height);
			imagejpeg($target_layer, $path . $file . "_thumb.jpg");
			return $file . "_thumb.jpg";
		case IMAGETYPE_GIF:
			$image_resource_id = imagecreatefromgif($path . $file);
			$target_layer = fn_resize($image_resource_id, $width, $height, $s_width, $s_height);
			imagegif($target_layer, $path . $file . "_thumb.gif");
			return $file . "_thumb.gif";
		case IMAGETYPE_PNG:
			$image_resource_id = imagecreatefrompng($path . $file);
			$target_layer = fn_resize($image_resource_id, $width, $height, $s_width, $s_height);
			imagepng($target_layer, $path . $file . "_thumb.png");
			return $file . "_thumb.png";
		default:
			return false;
	}
}

function GetImgProportions($img, $target_size)
{
	$source_properties = getimagesize($img);
	$img_width = $source_properties[0];	//source image width
	$img_height = $source_properties[1];	//source image height
	// echo $img_width . "x" . $img_height . "<br>";
	$final_size = array(0, 0); // (width, height)
	if($img_width > $img_height) {
		$final_size[0] = $target_size;
		$x = ($target_size * $img_height) / $img_width;
		$final_size[1] = round($x);
	} else if ($img_width < $img_height) {
		$final_size[1] = $target_size;
		$x = ($target_size * $img_width) / $img_height;
		$final_size[0] = round($x);
	} else {
		$final_size[0] = $target_size;
		$final_size[1] = $target_size;
	}
	return $final_size;
}

function fn_resize($image_resource_id,$target_width,$target_height,$width,$height) {
	$target_layer=imagecreatetruecolor($target_width,$target_height);
	imagecopyresampled($target_layer,$image_resource_id,0,0,0,0,$target_width,$target_height, $width,$height);
	return $target_layer;
}