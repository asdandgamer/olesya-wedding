<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#button1").click(function(){
        $.ajax({url: "ajax_test.txt", success: function(result){
            $("#div1").html(result);
        }});
    });
    $("#button2").click(function(){
        $.ajax({url: "ajax_retun_img_link.php", success: function(result){
            $("#div2").html(result);
        }});
    });
});
</script>
</head>
<body>

<div id="div1"><h2>Let jQuery AJAX Change This Text</h2></div>

<button id="button1">Get External Content</button>

<div id="div2">no text</div>

<button id="button2"></button>

</body>
</html>
