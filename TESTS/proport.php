<?php 
ini_set('display_errors', true); // !!!!! remove after debug  !!!!!!


//function GetImgProportions($img, $target_size); // [true] side type = width, [false] - height
function GetImgProportions($img, $target_size)
{
	$source_properties = getimagesize($img);
	$img_width = $source_properties[0];	//source image width
	$img_height = $source_properties[1];	//source image height

	// echo $img_width . "x" . $img_height . "<br>";

	$final_size = array(0, 0); // (width, height)
	if($img_width > $img_height) {
		$final_size[0] = $target_size;
		$x = ($target_size * $img_height) / $img_width;
		$final_size[1] = round($x);
	} else if ($img_width < $img_height) {
		$final_size[1] = $target_size;
		$x = ($target_size * $img_width) / $img_height;
		$final_size[0] = round($x);
	} else {
		$final_size[0] = $target_size;
		$final_size[1] = $target_size;
	}
	return $final_size;
}

print_r(GetImgProportions('./IMG/12.jpg', 400));

?>