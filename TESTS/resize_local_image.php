<?php
	ini_set('display_errors', true);

  // list($width, $height, $type, $attr) = getimagesize("./IMG/12.jpg");

  // echo "Image width " . $width;
  // echo "Image height " . $height;
  // echo "Image type " . $type;
  // echo "Attribute " . $attr;

  // // print_r(getimagesize("./IMG/12.jpg"));

//-------------------------------------------------------------------------------

	//Get Image Resource Id for Source Image

	$file = "./IMG/12.jpg"; 
	$source_properties = getimagesize($file);
	$image_type = $source_properties[2]; 
	if( $image_type == IMAGETYPE_JPEG ) {   
		$image_resource_id = imagecreatefromjpeg($file);  
	}
	elseif( $image_type == IMAGETYPE_GIF )  {  
		$image_resource_id = imagecreatefromgif($file);
	}
	elseif( $image_type == IMAGETYPE_PNG ) {
		$image_resource_id = imagecreatefrompng($file);
	}

	//Get Resource Id for Target Image Layer
	$target_width = 800;
	$target_height = 800;
	$target_layer=imagecreatetruecolor($target_width,$target_height);
	// Resizing and Reassembling
	imagecopyresampled($target_layer,$image_resource_id,0,0,0,0,$target_width,$target_height, $source_properties[0],$source_properties[1]);
	// Save Resized Image into Target Location
	imagejpeg($target_layer,"thumb.jpg");

	echo "work finished!";
?>