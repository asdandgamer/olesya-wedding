<?php
	ini_set('display_errors', true);

	$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
	$curDate = date("d/m/Y");
	$curTime = date("H:i:s");
	// $browser = get_browser(null, true);
	// print_r($browser);
	$browser = $_SERVER['HTTP_USER_AGENT'];
	if(isset($_SERVER['HTTP_REFERER'])) {
		echo $_SERVER['HTTP_REFERER'];
	} else {
		echo "direct";
	}
	// print_r($_SERVER);

	$text = $ip . " | " . $curDate . " @ " . $curTime . " | " . $browser . "\n" ;
	$fp = fopen("visitors.txt","a+") or die ("Error opening file in write mode!");
    fputs($fp, $text);
    fclose($fp) or die ("Error closing file!");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Olesya Wedding</title>
	<meta charset="utf-8">
	<style type="text/css">
		body {
			margin: 0 auto;
			align-content: center;
		}
		.wrap {
			background-image: url("./IMG/background.jpg");
			background-repeat: no-repeat;
			margin: 0 auto;
			max-width: 1100px;
		}
		.content {
			margin-top: 20px;
			margin-bottom: 20px;
			background-color: rgba(30, 30, 30, 0.5);
			border-radius: 20px;
			height: 620px;
			box-shadow: 0px 0px 20px #000;
		}
		.head {
			padding-top: 10px;
			padding-bottom: 10px;
			background-color: rgba(30, 30, 30, 0.8);
			border-radius: 20px 20px 0px 0px;
		}
		h1, h2 {
			margin: 0;
			color: #fff;
			text-align: center;
		}
		h3 {
			color: #dfc154;
			text-shadow: 0px 0px 2px black;
		}
	</style>
</head>
<body>
	<div class="wrap">
		<div class="content">
			<div class="head">
				<h1>Сайт знаходиться в розробці. Вибачте за незручності.</h1>
				<h2>Проте ви можете зв'язатись із нами за телефонами, або ж завітати за адресою:</h2>
			</div>
			<div style="float: left; padding-left: 20px;"><h3>+380975207184<br>+380974336853</h3></div>
			<div style="float: right; padding-right: 20px;"><h3>Тернопіль, бульв. Шевченка, 12<br>(ЦУМ 2 поверх, ліве крило)</h3></div>
		</div>
	</div>

	<!-- <span><?php echo  $text; ?></span> -->
</body>
</html>