<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form name="frmImageResize" action="upload.php" method="post" enctype="multipart/form-data">
		<input type="file" name="myImage" /> 
		<input type="submit" name="submit" value="Submit" />
	</form>
</body>
</html>

<?php 
ini_set('display_errors', true);

	if(isset($_POST["submit"])) {
		if(is_array($_FILES)) {
			$file = $_FILES['myImage']['tmp_name']; 
			$source_properties = getimagesize($file);
			$image_type = $source_properties[2]; 
			if( $image_type == IMAGETYPE_JPEG ) {   
				$image_resource_id = imagecreatefromjpeg($file);  
				$target_layer = fn_resize($image_resource_id,$source_properties[0],$source_properties[1]);
				imagejpeg($target_layer,$_FILES['myImage']['name'] . "_thump.jpg");
			}
			elseif( $image_type == IMAGETYPE_GIF )  {  
				$image_resource_id = imagecreatefromgif($file);
				$target_layer = fn_resize($image_resource_id,$source_properties[0],$source_properties[1]);
				imagegif($target_layer,$_FILES['myImage']['name'] . "_thump.gif");
			}
			elseif( $image_type == IMAGETYPE_PNG ) {
				$image_resource_id = imagecreatefrompng($file); 
				$target_layer = fn_resize($image_resource_id,$source_properties[0],$source_properties[1]);
				imagepng($target_layer,$_FILES['myImage']['name'] . "_thump.png");
			}
		}
	}
	function fn_resize($image_resource_id,$width,$height) {
		$target_width = 300;
		$target_height = 300;
		$target_layer=imagecreatetruecolor($target_width,$target_height);
		imagecopyresampled($target_layer,$image_resource_id,0,0,0,0,$target_width,$target_height, $width,$height);
		echo "work finished!";
		return $target_layer;
	}
?>