<?php /*
* file:		category.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.02.2018
*/ 
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once 'ENGINE/Engine.php';
  /*site settings TODO*/
// require_once 'ENGINE/ItemsManager.php';
$itemsManager = new ItemsManager();
$collections = array();
$categoryName = '';
if (isset($_GET['CatID'])) {
  $options = array(
      'options' => array(
          'default' => 0, // значение, возвращаемое, если фильтрация завершилась неудачей
          // другие параметры
          'min_range' => 0
      ),
      'flags' => FILTER_FLAG_ALLOW_OCTAL,
  );
  $var = filter_var($_GET['CatID'], FILTER_VALIDATE_INT, $options);
  if ($var == 0) {
    Site::Redirect('/404');
  }
  //----------------------------------------------------------
  $categoryName = $itemsManager->GetCategopryName($_GET['CatID']);
  $collections = $itemsManager->GetCollectionsNamesByCategory($_GET['CatID']);
}
$Title = $categoryName . ' | ' . Site::GetSiteName();
//-------------------------------------------------------------------
include_once 'TEMPLATE/header.php';	//header
include_once 'TEMPLATE/menu.php';	//menu
//include_once 'TEMPLATE/category.php';	//category
//-------------------------------------------------------------------
echo '<div class="content_wrap">';
  echo '<div class="breadcrumbs"><a href="/"><img src="img/home.png"></a> &gt; '. $categoryName . '</div>';
    echo '<div class="product_categories">
    <div class="text">' . $categoryName . '</div>';
      for ($i=0; $i < count($collections); $i++) {
        echo '<a href="/collection.php?CollID=' . $collections[$i][0] . '">';
        echo '<div class="product_category">
          <div class="label">  <span>'.$collections[$i][1] . '</span>
          </div>
          <div class="image">
            <img src="' . $itemsManager->GetCollectionImageLink($collections[$i][0]) . '">
          </div>
        </div>';
        echo '</a>';
      }
echo '</div>
  </div>';
//-------------------------------------------------------------------
include_once 'TEMPLATE/footer.php';	//footer
?>