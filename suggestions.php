<?php /*
* file:		suggestions.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		27.02.2018
*/ 
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once 'ENGINE/Engine.php';
//-------------------------------------------------------------------
$itemsManager = new ItemsManager();
$categoryName = 'Пропозиції';
$Title = $categoryName . ' | ' . Site::GetSiteName();
//-------------------------------------------------------------------
include_once 'TEMPLATE/header.php';	//header
include_once 'TEMPLATE/menu.php';	//menu
//-------------------------------------------------------------------
include_once 'TEMPLATE/suggestions.php';
//-------------------------------------------------------------------
include_once 'TEMPLATE/footer.php';	//footer
?>