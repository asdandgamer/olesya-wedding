<?php /* 
* file: 	Images.php @ AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		23.01.2018
*/
ini_set('display_errors', true);
require_once '../ENGINE/ImageManager.php';

$imgMan = new ImagesManager();
$imgID = NULL;
//_DUMP($imgMan->AddImage('12.jpg'));

//_DUMP($imgMan->GetOriginal(1));
//unlink($_SERVER["DOCUMENT_ROOT"] . $imgMan->GetOriginal(3));

// if (file_exists($_SERVER["DOCUMENT_ROOT"] . $imgMan->Gets200(3))) {
// 	echo "esist";
// } else {
// 	echo "no file";
// }
// _DUMP($imgMan->DeleteImage(2));
if(isset($_GET['id'])) {
	$imgID = $_GET['id'];
	switch ($_GET['size']) {
		case 's40':
			echo substr($imgMan->Gets40($imgID), 1);
			break;
		case 's200':
			echo substr($imgMan->Gets200($imgID), 1);
			break;
		case 's400':
			echo substr($imgMan->Gets400($imgID), 1);
			break;
		case 'original':
			echo substr($imgMan->GetOriginal($imgID), 1);
			break;
		default:
			# code...
			break;
	};
} else if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
	if ($_FILES)
	{
		$name = $_FILES['myfile']['name'];
		if(move_uploaded_file($_FILES['myfile']['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . '/IMG/' . $name)) {
			$imgID = $imgMan->AddImage($name);
			$response = array('id' => 0, 'link' => '');
			if($imgID) {
				$response['id'] = $imgID;
			} else {
				echo "Resizing failed!";
			}
			$s200 = $imgMan->Gets200($imgID);
			if($s200) {
				$response['link'] = substr($s200, 1);
			}
			echo json_encode($response);
		}
		else 
			echo "Upload failed!";
	}
} else echo "No data!!!";

function _DUMP($value)
{
	echo '<pre>';
	var_dump($value);
	echo '</pre>';
}

?>