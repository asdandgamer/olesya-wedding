<?php /*
* file:		Items.php @ AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		24.01.2018
*/ 
ini_set('display_errors', true); // !!! remove after DEBUG !!!

require_once '../ENGINE/Engine.php';
require_once '../ENGINE/ItemsManager.php';

$itemsManager = new ItemsManager(); 
if (!empty($_GET)) {
	if (isset($_GET["type"]) ) {
		if($_GET["type"]=='collections' && isset($_GET["category"])) {
			//echo json_encode($_GET);
			echo json_encode($itemsManager->GetCollectionsNamesByCategory($_GET["category"]));
		} else if($_GET["type"]=='collections' && isset($_GET["collectionID"])) {
			echo json_encode($itemsManager->GetFullCollectionInfo($_GET["collectionID"]));
		} else if($_GET["type"]=='categories') {
			echo json_encode($itemsManager->GetCategories());
		} else if($_GET["type"]=='manufacturers') {
			echo json_encode($itemsManager->GetManufacturersWID());
		}
	} else {
		echo json_encode($_GET);
	}
} 
else if (!empty($_POST)) {
	// Add Collection
	if( (isset($_POST["name"])) && 
		(isset($_POST["year"])) && 
		(isset($_POST["manufID"])) && 
		(isset($_POST["CategID"]))
	) {
		$itemsManager->AddCollection(
			$_POST["name"], 
			$_POST["year"], 
			$_POST["CategID"], 
			$_POST["manufID"],
			$_POST["CollIMG"]
		);
		echo json_encode(array('result' => 'OK'));
	}
	// Add Item
	else if((isset($_POST["name"])) && (isset($_POST["price"])) && (isset($_POST["collectionID"]))) {
		if (isset($_POST["imagesID"])) {
			$itemsManager->AddItem($_POST["name"], $_POST["collectionID"], $_POST["price"], implode(", ", $_POST["imagesID"]), '',Site::getUserId(), date("Y-m-d H:i:s"));
			echo json_encode($_POST); //echo json_encode(array('lastID' => $itemsManager->GetLastItemID()));
		} else {
			echo json_encode($_POST);
		}
	} else {
		json_encode(array('result' => 'Invalid data'));
	}
} else {
	echo json_encode(array('1' => 'failure'));
}
// else if($itemsManager->ErrMsg()) {
// 	echo $itemsManager->ErrMsg();
// } else {
// 	//echo 'connected!';

// 	// $itemsManager->AddCategopry('дитячі');
// 	//_DUMP($itemsManager->GetCategories());

// 	// $itemsManager->AddManufacturer('Pentelei');
// 	// _DUMP($itemsManager->GetManufacturers());

// 	// _DUMP($itemsManager->GetManufacturerName('1'));
// 	// _DUMP($itemsManager->GetCategopryName('4'));

// 	//				 AddCollection($name, $year, $categoryID, $manufacturerID)
// 	// $itemsManager->AddCollection('Collection Brilliance of bride', 2018, 1, 3);
// 	// _DUMP($itemsManager->GetCollectionsNames());
// 	// _DUMP($itemsManager->GetCollectionName(2));
// 	// _DUMP($itemsManager->GetCollectionInfo(2));
// 	// _DUMP($itemsManager->GetFullCollectionInfo(2));


// 	//				AddItem($name, $collectionID, $price=NULL, $imagesIDs=NULL, $colorsIDs=NULL)
// 	//$itemsManager->AddItem('2200', 2);
// 	_DUMP($itemsManager->GetItemInfo(1));

// 	//				EditItem($id, $name, $collectionID, $price=0, $imagesIDs='', $colorsIDs='')
// 	//$itemsManager->EditItem(1, '2200', 2, 6500);
// 	_DUMP($itemsManager->GetItemInfo(1));


// 	// $itemsManager->AddItem();
// }

function _DUMP($value)
{
	echo '<pre>';
	var_dump($value);
	echo '</pre>';
}


?>