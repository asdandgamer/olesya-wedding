<?php /*
* file:		Statistic.php @ AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		24.02.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once '../ENGINE/Engine.php';
require_once '../ENGINE/Statistic.php';

$statistic = new StatisticManager();

if (!empty($_POST)) {
	$ipinfo = '';
	if(isset($_POST["city"]) && isset($_POST["region"]) && isset($_POST["country"])) {
		$ipinfo = array('city' => $_POST["city"],
						'region' => $_POST["region"],
						'country' => $_POST["country"]);
	}
	if ($_POST["info"]=='1') {
		$statistic->TryWrite($_POST["ref"], $ipinfo, $_POST["latitude"], $_POST["longitude"]);
	} else if($_POST["info"]=='2') {
		$latitude = explode(",", $_POST["loc"])[0];
		$longitude = explode(",", $_POST["loc"])[1];
		$statistic->TryWrite($_POST["ref"], $ipinfo, $latitude, $longitude);
	} else if ($_POST["info"]=='3') {
		$statistic->TryWrite($_POST["ref"], $ipinfo, NULL, NULL);
	}
	echo json_encode($_POST);
} else {
	echo json_encode(array('data' => 'failure'));
}

?>