<?php /*
* file:		login.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		06.02.2018
*/ 
	ini_set('display_errors', true); // !!! remove after DEBUG !!!

	include_once 'ENGINE/Site.php';

if (!($_POST) && (isset($_GET)) && !(isset($_GET["data"]))) {
	include_once 'TEMPLATE/login.php';
} else if (isset($_POST["login"]) && isset($_POST["password"])) {
	$status = Site::LogIn($_POST["login"], $_POST["password"]);
	print_r($status);
	// if($result) { 	// login good
	// 	echo true;
	// } else {		// login failed
	// 	echo false;
	// }
} else if(isset($_GET["data"]) && $_GET["data"]=='LogOut') {
	Site::LogOut();
	echo true;
} else {
	echo "Empty Page";
}
	
?>