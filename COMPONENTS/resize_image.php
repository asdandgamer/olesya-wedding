<?php /*
file:	resize_image.php
autor:	asdandgamer
e-mail: asdandgamer@gmail.com
create:	23.01.2018
---------------------------------------------------------------
description:	Resizes image file.
*/
ini_set('display_errors', true);
require_once '../ENGINE/ResizeImage.php';

if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
	if ($_FILES)
	{
		$name = $_FILES['myfile']['name'];
		if(move_uploaded_file($_FILES['myfile']['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . '/IMG/' . $name)) {
			$saveFolder = '/IMG/400x400/';
			$resizedImage = IResizeImage::ResizeImage($_SERVER["DOCUMENT_ROOT"] . '/IMG/', $name, 400, ($_SERVER["DOCUMENT_ROOT"] . $saveFolder));
			if($resizedImage) {
				$image = $saveFolder . $resizedImage;
				echo "Загружаемое изображение '$image'<br><img src='$image'>";
			}
			else
				echo "Resizing failed!";
		}
		else 
			echo "Upload failed!";
	}
} else echo "string";

?>