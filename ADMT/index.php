<?php /*
* file:		index.php @ ADMT
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		29.05.2018
*/ 
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Engine.php';
//-------------------------------------------------------------------
require_once  $_SERVER["DOCUMENT_ROOT"]."/banned.php";
$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
if(bannedIP($ip)) Site::Redirect('/banned.php');
//-------------------------------------------------------------------
if (Site::getLogInStatus()) require_once 'main.php';
else Site::Redirect('/login.php?location=admt');

?>