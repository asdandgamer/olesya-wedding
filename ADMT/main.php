<?php /*
* file:		index.php @ ADMT
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		29.05.2018
*/ 
ini_set("display_errors", true); // !!! remove after DEBUG !!!
require_once '../ENGINE/Site.php';
//-------------------------------------------------------------------
function TextToTable($inStr) {
	// $inStr = RemoveLastEndl($inStr);
	$inStr = substr($inStr, 0, -1);
	//---------------------------------------------------------------
	$tableHead = "<table class=\"visits_table\"><tbody><tr><td>";
	$tableFoot = "</td></tr></tbody></table>";
	while(strpos($inStr, PHP_EOL)) {
		$inStr = str_replace(PHP_EOL, "</td></tr><tr><td>", $inStr);
    }
    $pos = preg_match('/[ ]+(?![a-z])/', $inStr);
	while($pos) {
        $inStr = preg_replace('/[ ]+(?![a-z])/', "</td><td>", $inStr);
        $pos = preg_match('/[ ]+(?![a-z])/', $inStr);
	}
	return $tableHead . $inStr . $tableFoot;
}
function RemoveLastEndl($text) {
    if(substr($text, -1) == '\n') // get last character of string
        $text = substr($text, 0, -1); // remove last character of string
    return $text;
}
//-------------------------------------------------------------------
require_once 'TEMPLATE/ADMT_main.php';

?>