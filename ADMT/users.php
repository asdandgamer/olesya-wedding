<?php /*
* file:		users.php @ ADMT
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		01.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Engine.php';
require_once 'CORE/UserManager.php';

if (!Site::getLogInStatus()) {
    Site::Redirect('/login.php?location=admt_users');
} else if(Site::GetCurrentUserLevel() < USR_LEVEL_ADMIN) {
	require_once 'TEMPLATE/Restricted.php';
} else {
    require_once 'TEMPLATE/ADMT_users.php';
}

?>