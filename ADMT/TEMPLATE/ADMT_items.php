<?php /*
* file:	    items.php @ ADMT : TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Welcome to Admin Tools</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
	<script src="JS/ADMT_main.js"></script>
	<script src="JS/ADMT_items.js"></script>
</head>
<body>
<div id="deleteitem" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Видалити</span>
			<input type="button" value="&#215;" onclick="CloseDeleteItem()" title="Close">
		</div>
		<div class="window_content">
			<span>Ви впевнені що хочете видалити</span>
			<span id="delitemname" style="font-weight: bold;"></span>
			<br>
			<input type="checkbox" id="delImgs">
			<label for="delImgs">Видалити разом з картинками</label>
			<hr style="margin: 5px 0px;">
			<input class="ok_btn" id="delitembtn" type="button" value="Так" onclick="DeleteItem()">
			<input class="cancel_btn" type="button" value="Ні" onclick="CloseDeleteItem()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div id="itemprev" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span></span>
			<input type="button" value="&#215;" onclick="CloseItemPrev()" title="Close">
		</div>
		<div class="window_content">
			<label for="piName">Назва</label>
			<input type="text" id="piName" disabled>
			<div class="imgs_container"></div>
			<div id="prevdebug"></div>
			<hr style="margin: 5px 0px;">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
    <div class="admt_wr">
		<div class="admt_block">
			<div class="block_header"><h2>Items</h2></div>
			<hr>
			<div class="block_content">
			<table class="user_list">
				<tbody>
					<tr>
						<td>ID</td>
						<td>Назва</td>
						<td>Категорія</td>
						<td>Колекція</td>
						<td>Виробник</td>
						<td>Рік</td>
						<td>Ціна</td>
						<!-- <td>Картинки</td>
						<td>Кольори</td> -->
						<td>Автор</td>
						<td>Додано</td>
						<td>Керування</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><input class="new_btn" type="button" onclick="OpenAddItem()" value="Додати"></td>
					</tr>
					<?php $itemsManager = new ItemsManager(); 
						$items = $itemsManager->GetItemsList();
						for ($i=count($items)-1; $i >= 0; $i--) {
							$iID = $items[$i][0];
							$iName = $items[$i][1];
							$iColID = $items[$i][2];
							$collInfo = $itemsManager->GetFullCollectionInfo($iColID);
							$iColName = $collInfo["name"];
							$iCategoryName = $collInfo["category"];
							$iMfgName = $collInfo["manufacturer"];
							$iYear = $collInfo["year"];
							$iPrice=$items[$i][3];
							//$iImageList=$items[$i][4];
							//$iColors=$items[$i][5];
							$iAutorID=$items[$i][6];
							$iAutorName = $itemsManager->GetAutorByID($iAutorID);
							$iDateAdded = $items[$i][7];
							//-------------------------------------------------
							echo '<tr ondblclick="OpenPreview('.$iID.')">'.PHP_EOL;
							echo '<td>'. $iID .'</td>'.PHP_EOL;							// ID
							echo '<td>'. $iName .'</td>'.PHP_EOL;						// Назва
							echo '<td>'. $iCategoryName .'</td>'.PHP_EOL;				// Категорія
							echo '<td value="'.$iColID.'">'. $iColName .'</td>'.PHP_EOL;// Колекція
							echo '<td>'. $iMfgName .'</td>'.PHP_EOL;						// Виробник
							echo '<td>'. $iYear .'</td>'.PHP_EOL;						// Рік
							echo '<td>'. $iPrice .'</td>'.PHP_EOL;						// Ціна
							echo '<td value="'.$iAutorID.'">'. $iAutorName .'</td>'.PHP_EOL; // Автор
							echo '<td>'. $iDateAdded .'</td>'.PHP_EOL;					// Додано
							echo '<td>'.										// Керування
								'<input class="ok_btn" type="button" onclick="EditItem('.$iID.')" value="Редагувати"> '.
								'<input class="cancel_btn" type="button" onclick="OpenDeleteItem('.$iID.',\''.$iName.'\')" value="Видалити">'.
								'</td>'.PHP_EOL;
							echo "</tr>".PHP_EOL;
						}
					?>
				</tbody>
				</table>
            </div>
		</div>
	</div>
</div>
</body>