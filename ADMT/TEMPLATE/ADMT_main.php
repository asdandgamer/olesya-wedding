<?php /*
* file:		ADMT_main.php @ ADMT : TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		29.05.2018
*/ 
ini_set("display_errors", true);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Welcome to Admin Tools</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
    <script src="JS/ADMT_main.js"></script>
</head>
<body>
	<!-- <form class="loginform">
				<div class="std_field">
					<label class="std_size std_label">Логін:</label><input id="login" class="std_size std_edit" type="text" name="login" />
				</div>
				<div class="std_field">
					<label class="std_size std_label">Пароль:</label><input id="passwd" class="std_size std_edit" type="password" name="password" />
				</div>-
				<div class="std_button_container coll_btn_container">
					<input class="std_button ok_btn" type="button" value="Увійти" name="log_in" onclick="LogIn()" />
				</div>
			</form> -->
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
	<div class="admt_wr">
		<div class="admt_block">
			<div class="block_header"><h2>Server info:</h2></div>
			<hr>
			<div class="block_content">
				<?php 	echo shell_exec("lscpu | grep 'Model name'"); echo "<br>";
						echo shell_exec("lscpu | grep 'Core(s'"); echo "<br>";
						echo shell_exec("lscpu | grep 'MHz'");
						// echo "<br>";
						// echo shell_exec("grep MemTotal /proc/meminfo");
						echo "<hr>RAM ";
						echo exec("lshw -c memory | grep size"); echo "<br>";
						echo exec("df -h | grep 'Filesystem'"); echo "<br>";
						echo exec("df -h | grep '/dev/sd'"); echo "<hr>";
						echo TextToTable(shell_exec("df -h"));
						echo TextToTable(shell_exec("free -h"));
						// echo TextToTable(shell_exec('htop'));
				?>
			</div>
		</div>
	</div>
</div>
</body>
</html>