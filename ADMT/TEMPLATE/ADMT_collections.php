<?php /*
* file:	    ADMT_manfacturers.php @ ADMT : TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		20.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Welcome to Admin Tools</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
    <script src="JS/ADMT_main.js"></script>
    <script>
function OpenAddColl() {
	$('#addcoll').show();
	//-----------------------------------
	LoadManufacturers();
	LoadCategories();
}
function CloseAddColl() {
    $('#collname').val('');
    //---------------------------------------------------------------
    $('.errtxt').empty();
    $('.errtxt').hide();
    //---------------------------------------------------------------
    $('#addcoll').hide();
}
function AddColl() {
    UploadCollImage();
}
function UploadCollImage() {
	var data = {
		// command: 'addcollimg',
		collImgFile: $('#collimg')[0].files[0]
	};
	var formData = new FormData();
	formData.append('collImgFile', $('#collimg')[0].files[0]);
	$.ajax({
		type: "POST",
		url: 'AJAX/ADMT_collections.php',
		data: formData,
		processData: false,
		contentType: false,
		dataType: "json",
		xhr: function() {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = parseInt((evt.loaded / evt.total) * 100);
					$('#collprogress').text(percentComplete + '%');
					$('#collprogress').css('width', percentComplete + '%');
				}
			}, false);
			return xhr;
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			//var data1 = JSON.parse(data);
			if(data.status == true) SendCollectionData(data.imgPath);
			else ShowError(dataa.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}
function SendCollectionData(imagePath) {
	var data = {
		command: 'addcoll',
		collName: $('#collname').val(),
		collYear: $('#collyear').val(),
		collMfg: $('#collmfg').val(),
		collCtg: $('#collctg').val(),
		collImg: imagePath
	};
	// alert(data.collImg);
	// return;
	$.ajax({
		type: "POST",
		url: 'AJAX/ADMT_collections.php',
		data: data,
		dataType: "json",
		success: function(data) {
			if(data.status == true) window.location.href = "/ADMT/collections.php";
			else ShowError(data.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}
function LoadManufacturers() {
	$.ajax({
		type: "GET",
		url: "AJAX/ADMT_collections.php",
		dataType: "json",
		data: { type: 'manufacturers' },
		success: function(data) {
			$('#collmfg').empty();
			for (var i = 0; i < data.length; i++) {
				var opt = document.createElement('option');
				opt.value = (Object.values(data))[i][0];
				opt.innerHTML = (Object.values(data))[i][1];
				$('#collmfg').append(opt);
			}
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
function LoadCategories() {
	// if (!categoryIsEmpty) { LoadCollections(); return; }
	$.ajax({
		type: "GET",
		url: "AJAX/ADMT_collections.php",
		dataType: "json",
		data: { type: 'categories' },
		success: function(data) {
			categoryIsEmpty = false;
			$('#collctg').empty();
			for (var i = 0; i < data.length; i++) {
				var opt = document.createElement('option');
				opt.value = (Object.values(data))[i][0];
				opt.innerHTML = (Object.values(data))[i][1];
				$('#collctg').append(opt);
			}	
			// if(FirstLoadPage) { LoadCollections(); };
		},
		error: function(xhr) {
			console.error(xhr);
		}
	});
}
//-------------------------------------------------------------------
function OpenDeleteColl(collID, collName) {
	$('#delcollname').text(collName);
	$('#delcollbtn').removeAttr('onclick');
	$('#delcollbtn').attr('onClick', 'DeleteColl('+collID+')');
	//---------------------------------------------------------------
	$('#delcoll').show();
}
function CloseDeleteColl() {
	$('.errtxt').empty();
	$('.errtxt').hide();
	$('#delcoll').hide();
}
function DeleteColl(collID) {
	var data = {
		command: 'delcoll',
		collID: collID
	};
	$.ajax({
		type: "POST",
		url: 'AJAX/ADMT_collections.php',
		data: data,
		dataType: "json",
		success: function(data) {
			if(data.status == true) window.location.href = "/ADMT/collections.php";
			else ShowError(data.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}
//-------------------------------------------------------------------
function ShowError(html) {
	$('.errtxt').html(html);
	$('.errtxt').fadeIn();
}
	</script>
	<style>
.window_progress_wr {
	border: 1px solid grey;
	width: 100%;
	background-color: #bbb;
}
.window_progress {
	background-color: #008080;
	text-align: center;
	color: white;
	text-shadow: 0px 0px 1px white;
	width: 0%;
}
	</style>
</head>
<body>
<div id="delcoll" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Видалити колекцію</span>
			<input type="button" value="&#215;" onclick="CloseDeleteColl()" title="Close">
		</div>
		<div class="window_content">
			<span>Ви впевнені що хочете видалити колекцію</span>
			<span id="delcollname" style="font-weight: bold;"></span>
			<hr style="margin: 5px 0px;">
			<input id="delcollbtn" type="button" value="Так" onclick="DeleteCool()">
			<input type="button" value="Ні" onclick="CloseDeleteColl()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div id="addcoll" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Додати виробника</span>
			<input type="button" value="&#215;" onclick="CloseAddColl()" title="Close">
		</div>
		<div class="window_content">
			<label for="collname">Назва: </label><input type="text" id="collname"><br>
			<label for="collyear">Рік: </label><input type="number" id="collyear"><br>
			<label for="collmfg">Виробник: </label><select id="collmfg"></select><br>
			<label for="collctg">Категорія: </label><select id="collctg"></select><br>
			<label for="collimg">Зображення: </label><input type="file" id="collimg">
			<div class="window_progress_wr">
				<div id="collprogress" class="window_progress" role="progressbar">0%</div>
			</div>
			<hr style="margin: 5px 0px;">
			<input class="ok_btn" type="button" value="Додати" onclick="AddColl()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
    <div class="admt_wr">
		<div class="admt_block">
			<div class="block_header"><h2>Колекції</h2></div>
			<hr>
			<div class="block_content">
			<table class="user_list">
				<tbody>
					<tr>
						<td>ID</td>
                        <td>Назва</td>
                        <td>Рік</td>
                        <td>Категорія</td>
                        <td>Виробник</td>
                        <td>Картинка</td>
						<td>Керування</td>
					</tr>
					<tr>
						<td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
						<td><input class="new_btn" type="button" onclick="OpenAddColl()" value="Додати"></td>
					</tr>
					<?php $itemsManager = new ItemsManager(); 
                        $colls = $itemsManager->GetAllCollections();
                        //_DUMP($colls);
						for ($i=count($colls)-1; $i >= 0; $i--) {
							$cID = $colls[$i][0];
                            $cName = $colls[$i][1];
                            $cYear = $colls[$i][2];
                            $cCatName = $itemsManager->GetCategopryName($colls[$i][3]);
                            $cMfgName = $itemsManager->GetManufacturerName($colls[$i][4]);
                            $cImgSrc = $colls[$i][5];
							//-------------------------------------------------
							echo '<tr>'.PHP_EOL;
							echo '<td>'. $cID .'</td>'.PHP_EOL;							// ID
                            echo '<td>'. $cName .'</td>'.PHP_EOL;						// Назва
                            echo '<td>'. $cYear .'</td>'.PHP_EOL;
                            echo '<td>'. $cCatName .'</td>'.PHP_EOL;
                            echo '<td>'. $cMfgName .'</td>'.PHP_EOL;
                            echo '<td><img src="/'. $cImgSrc .'" width="150" height="150"></td>'.PHP_EOL;
							echo '<td>'.										// Керування
								'<input class="cancel_btn" type="button" onclick="OpenDeleteColl('.$cID.',\''.$cName.'\')" value="Видалити">'.
								'</td>'.PHP_EOL;
							echo "</tr>".PHP_EOL;
						}
					?>
				</tbody>
				</table>
            </div>
		</div>
	</div>
</div>
</body>