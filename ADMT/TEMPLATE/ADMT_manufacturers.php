<?php /*
* file:	    ADMT_manfacturers.php @ ADMT : TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Welcome to Admin Tools</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
    <script src="JS/ADMT_main.js"></script>
    <script>
function OpenAddMfg() {
	$('#addmfg').show();
}
function CloseAddMfg() {
    $('#mfgname').val('');
    //---------------------------------------------------------------
    $('.errtxt').empty();
    $('.errtxt').hide();
    //---------------------------------------------------------------
    $('#addmfg').hide();
}
function AddMfg() {
    var data = {
		command: 'addmfg',
		mfgName: $('#mfgname').val()
	};
	$.ajax({
		type: "POST",
		url: 'AJAX/ADMT_manufacturers.php',
		data: data,
		dataType: "json",
		success: function(data) {
			if(data.status == true) window.location.href = "/ADMT/manufacturers.php";
			else ShowError(data.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}
//-------------------------------------------------------------------
function OpenDeleteMfg(mfgID, mfgName) {
	$('#delmfgname').text(mfgName);
	$('#delmfgbtn').removeAttr('onclick');
	$('#delmfgbtn').attr('onClick', 'DeleteMfg('+mfgID+')');
	//---------------------------------------------------------------
	$('#delmfg').show();
}
function CloseDeleteMfg() {
	$('.errtxt').empty();
	$('.errtxt').hide();
	$('#delmfg').hide();
}
function DeleteMfg(mgfID) {
	var data = {
		command: 'delmfg',
		mgfID: mgfID
	};
	$.ajax({
		type: "POST",
		url: 'AJAX/ADMT_manufacturers.php',
		data: data,
		dataType: "json",
		success: function(data) {
			if(data.status == true) window.location.href = "/ADMT/manufacturers.php";
			else ShowError(data.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}
//-------------------------------------------------------------------
function ShowError(html) {
	$('.errtxt').html(html);
	$('.errtxt').fadeIn();
}
	</script>
</head>
<body>
<div id="delmfg" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Видалити виробника</span>
			<input type="button" value="&#215;" onclick="CloseDeleteMfg()" title="Close">
		</div>
		<div class="window_content">
			<span>Ви впевнені що хочете видалити виробника</span>
			<span id="delmfgname" style="font-weight: bold;"></span>
			<hr style="margin: 5px 0px;">
			<input id="delmfgbtn" type="button" value="Так" onclick="DeleteMfg()">
			<input type="button" value="Ні" onclick="CloseDeleteMfg()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div id="addmfg" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Додати виробника</span>
			<input type="button" value="&#215;" onclick="CloseAddMfg()" title="Close">
		</div>
		<div class="window_content">
			<label for="mfgname">Назва</label>
			<input type="text" id="mfgname">
			<hr style="margin: 5px 0px;">
			<input class="ok_btn" type="button" value="Додати" onclick="AddMfg()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
    <div class="admt_wr">
		<div class="admt_block">
			<div class="block_header"><h2>Виробники</h2></div>
			<hr>
			<div class="block_content">
			<table class="user_list">
				<tbody>
					<tr>
						<td>ID</td>
						<td>Назва</td>
						<td>Керування</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><input class="new_btn" type="button" onclick="OpenAddMfg()" value="Додати"></td>
					</tr>
					<?php $itemsManager = new ItemsManager(); 
						$mfgs = $itemsManager->GetManufacturersWID();
						for ($i=count($mfgs)-1; $i >= 0; $i--) {
							$mID = $mfgs[$i][0];
							$mName = $mfgs[$i][1];
							//-------------------------------------------------
							echo '<tr>'.PHP_EOL;
							echo '<td>'. $mID .'</td>'.PHP_EOL;							// ID
							echo '<td>'. $mName .'</td>'.PHP_EOL;						// Назва
							echo '<td>'.										// Керування
								'<input class="cancel_btn" type="button" onclick="OpenDeleteMfg('.$mID.',\''.$mName.'\')" value="Видалити">'.
								'</td>'.PHP_EOL;
							echo "</tr>".PHP_EOL;
						}
					?>
				</tbody>
				</table>
            </div>
		</div>
	</div>
</div>
</body>