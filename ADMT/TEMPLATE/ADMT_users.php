<?php /*
* file:		ADMT_users.php @ ADMT : TEMPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		04.09.2018
*/
ini_set("display_errors", true);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Welcome to Admin Tools</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
	<script src="JS/ADMT_main.js"></script>
	<script>
		var delColId = -1;		// containing column column id before deleting user. 
								//Changes on opening delete user window | div[id="deleteuser"]
		var startUserLevel = -1; // containing user level before change
								//Changes on opening edit window | OpenEditUser(colId);
		//---------------------------------------------------------------------
		function OpenAddUser() {
			$('.window_controls').children('span').text('Додати користувача');
			$('#addusrbtn').show();
			$('#editusrbtn').hide();
			$('#uid').val('');
			//-----------------------------------------------------------------
			$('#adduser').show();
		}
		function CloseAddUser() {
			$('#adduser').hide();
			$('.errtxt').hide();
			//*-----------------------
			$('#name').val('');
			$('#pass1').val('');
			$('#pass2').val('');
			$('#level').val(1);
			//------------------------
			$('#name').attr('disabled', false);
			$('#level').attr('disabled', false);
			//------------------------
			if($('#level option[value="9"]').text()!='')
				$('#level option[value="9"]').remove();
		}
		//---------------------------------------------------------------------
		function OpenEditUser(colId) {
			//colId - column id
			$('.window_controls').children('span').text('Редагувати користувача');
			$('#addusrbtn').hide();
			$('#editusrbtn').show();
			//-----------------------------------------------------------------
			var user_id = $('.user_list').find('tr').eq(colId+1).children('td').eq(0).text();
			$('#uid').val(user_id);
			//-----------------------------------------------------------------
			var login = $('.user_list').find('tr').eq(colId+1).children('td').eq(1).text();
			var level = $('.user_list').find('tr').eq(colId+1).children('td').eq(3).text();
			startUserLevel = level;
			$('#name').val(login);
			$('#name').attr('disabled', true);
			//-----------------------------------------------------------------
			if(level==9) {
				$('#level').attr('disabled', true);
				$('#level').append($('<option>', { 
					value: 9, text: 'Адміністратор'}));
			}
			$('#level').val(level);
			//-----------------------------------------------------------------
			$('#adduser').show();
		}
		function OpenDeleteUser(colId) {
			var login = $('.user_list').find('tr').eq(colId+1).children('td').eq(1).text();
			var level = $('.user_list').find('tr').eq(colId+1).children('td').eq(3).text();
			if(level==9) {
				delColId = -1;
				$('#del_usr_question').text("Ви не можете видалити Адміністратора!");
				$('#delusrname').text('');
				$('#delusrbtn').attr('disabled', true);
			} else {
				delColId = colId;
				$('#del_usr_question').text("Ви впевнені що хочете идалити користувача з ім'ям ");
				$('#delusrname').text(login);
				$('#delusrbtn').attr('disabled', false);
			}
			//-----------------------------------------------------------------
			$('#deleteuser').show();
		}
		function CloseDeleteUser() {
			$('#deleteuser').hide();
			$('.errtxt').hide();
		}
		//---------------------------------------------------------------------
		function AddUser() {
			// check password ----------------------------------------
			if($('#pass1').val() != $('#pass2').val()) {
				ShowError('Паролі відрізняються'); return; 
			} else $('.errtxt').hide();
			//--------------------------------------------------------
			var data = {
				command: 'adduser',
				login: $('#name').val(),
				pass: $('#pass1').val(),
				level: $('#level').val()
			};
			$.ajax({
				type: "POST",
				url: 'AJAX/ADMT_users.php',
				data: data,
				dataType: "text json",
				success: function(data) {
					if(data.status == true) 
						window.location.href = "/ADMT/users.php";
					else ShowError(data.reason);
				},
				error: function(xhr) {
					console.error(xhr);
					ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
				}
			});
		}
		function DeleteUser() {
			var user_id = $('.user_list').find('tr').eq(delColId+1).children('td').eq(0).text();
			//-------------------------------------------------------------------------------
			var data = {
				command: 'deleteuser',
				userID: user_id
			};
			$.ajax({
				type: "POST",
				url: 'AJAX/ADMT_users.php',
				data: data,
				dataType: "json",
				success: function(data) {
					if(data.status == true) {
						window.location.href = "/ADMT/users.php";
					} else {
						ShowError(data.reason);
					}
				},
				error: function(xhr) {
					console.error(xhr);
					ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
				}
			});
		}
		function EditUser() {
			if($('#pass1').val() !='' && $('#pass2').val() != '') {
				if($('#pass1').val() != $('#pass2').val()) {
					ShowError('Паролі відрізняються'); return; 
				} else $('.errtxt').hide();
				//-------------------------------------------------------------
				var data = {
					command: 'setpass',
					userID: $('#uid').val(),
					pass: $('#pass1').val(),
					level: $('#level').val()
				};
				$.ajax({
					type: "POST",
					url: 'AJAX/ADMT_users.php',
					data: data,
					dataType: "json",
					success: function(data) {
						if(data.status == true)
							window.location.href = "/ADMT/users.php";
						else ShowError(data.reason);
					},
					error: function(xhr) {
						console.error(xhr);
						ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
						return;
					}
				});
			}
			var level = $('#level').val();
			if(startUserLevel != level) {
				var data = {
					command: 'setlvl',
					userID: $('#uid').val(),
					level: $('#level').val()
				};
				$.ajax({
					type: "POST",
					url: 'AJAX/ADMT_users.php',
					data: data,
					dataType: "json",
					success: function(data) {
						if(data.status == true) {
							window.location.href = "/ADMT/users.php";
						} else {
							ShowError(data.reason);
						}
					},
					error: function(xhr) {
						console.error(xhr);
						ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
						return;
					}
				});
			}
		}
		//---------------------------------------------------------------------
		function ShowError(html) {
			$('.errtxt').html(html);
			$('.errtxt').fadeIn();
		}
	</script>
	<style>
		
	</style>
</head>
<body>
<div id="deleteuser" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Видалити користувача</span>
			<input type="button" value="&#215;" onclick="CloseDeleteUser()" title="Close">
		</div>
		<div class="window_content">
			<span id="del_usr_question">Ви впевнені що хочете идалити користувача з ім'ям </span>
			<span id="delusrname" style="font-weight: bold;"></span>
			<br>
			<input id="delusrbtn" type="button" value="Так" onclick="DeleteUser()">
			<input id="closedelusrbtn" type="button" value="Ні" onclick="CloseDeleteUser()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div id="adduser" class="window_wr">
	<div class="window">
		<div class="window_controls">
			<span>Додати користувача</span>
			<input type="button" value="&#215;" onclick="CloseAddUser()" title="Close">
		</div>
		<div class="window_content">
			<label for="uid">ID: </label>
			<input type="text" id="uid" disabled>
			<br>
			<label for="name">Нікнейм: </label>
			<input type="text" id="name" >
			<br>
			<label for="name">Пароль: </label>
			<input type="password" id="pass1">
			<br>
			<label for="name">Повторіть пароль: </label>
			<input type="password" id="pass2">
			<br>
			<label for="name">Права: </label>
			<select id="level">
                <option value="1">Гість</option>
                <option value="2">Користувач</option>
                <option value="4">Модератор</option>
			</select>
			<hr style="margin: 5px 0px;">
			<input id="addusrbtn" type="button" value="Додати" onclick="AddUser()">
			<input id="editusrbtn" type="button" value="Редагувати" onclick="EditUser()">
			<div class="errtxt"></div>
		</div>
	</div>
</div>
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
	<div class="admt_wr">
		<div class="admt_block">
			<div class="block_header"><h2>Користувачі</h2></div>
			<hr>
			<div class="block_content">
				<table class="user_list">
				<tbody>
					<tr>
						<td>ID</td>
						<td>Нікнейм</td>
						<td>Hash</td>
						<td>Права</td>
						<td>Керування</td>
					</tr>
					<?php $usrMan = new UserManager(); 
						$users = $usrMan->GetAllUsers();
						for ($i=0; $i < count($users); $i++) {
							echo "<tr>";
							for ($j=0; $j < count($users[$i]); $j++) { 
								echo "<td>".$users[$i][$j]."</td>";
							}
							echo '<td>'.
								'<input class="ok_btn" type="button" onclick="OpenEditUser('.$i.')" value="Редагувати"> '.
								'<input class="cancel_btn" type="button" onclick="OpenDeleteUser('.$i.')" value="Видалити">'.
								'</td>';
							echo "</tr>";
						}
					?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><input class="new_btn" type="button" onclick="OpenAddUser()" value="Додати"></td>
					</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</body>