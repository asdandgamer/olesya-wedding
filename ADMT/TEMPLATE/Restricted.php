<?php /*
* file:		Restricted.php @ ADMT : TEPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		17.09.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Restricted area</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
    <script src="JS/ADMT_main.js"></script>
    <style>
        .resarea_wr {
            width: 550px;
            margin: 0 auto;
            border: 2px solid red;
            border-radius: 30px;
            padding: 5px;
            font-weight: bold;
            text-align: center;
        }
        .ra_top {
            background-color: red;
            border-radius: 25px 25px 0px 0px;
            width: 100%;
        }
        .ra_top>div {
            color: white;
            width: 100%;
            font-size: 60px;
        }
        .ra_bottom {
            font-size: 40px;
        }
        .ra_bottom>div {
            padding: 25px 0px;
        }
    </style>
</head>
<body>
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
    <div class="admt_wr">
		<div class="admt_block">
            <div style="height: 100px;"></div>
            <div class="resarea_wr">
                <div class="ra_top">
                    <div>RESTRICTED AREA</div>
                </div>
                <div class="ra_bottom">
                    <div>AUTHORISED PERSONNEL ONLY</div>
                </div>
            </div>
		</div>
	</div>
</div>
</body>