<?php /*
* file:		ADMT_EditItem.php @ ADMT : TEPLATE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		17.09.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Welcome to Admin Tools</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="/JS/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_menu.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="CSS/ADMT_userbar.css" />
	<script src="JS/ADMT_main.js"></script>
	<script>
		$(document).ready(function () {
			$str = '<a class="lmenu_link_selected">\n<div class="menu_item_logo">E</div><div class="menu_item_label">dit</div>\n</a>';
			$('.left_menu').append($str);
		});
	</script>
</head>
<body>
<div class="wrap">
	<?php include_once 'Modules/LeftMenu.php'; ?>
	<?php include_once 'Modules/UserBar.php'; ?>
    <div class="admt_wr">
		<div class="admt_block">
			<div class="block_header"><h2>Edit item</h2></div>
			<hr>
			<div class="block_content">
                <h2>This function is not ready yet!</h2>
            </div>
		</div>
	</div>
</div>
</body>