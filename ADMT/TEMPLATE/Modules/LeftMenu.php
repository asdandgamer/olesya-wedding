<?php /*
* file:		LeftMenu.php @ ADMT : TEMPLATE : Modules
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		06.09.2018
*/ ?>
<div class="left_menu">
	<a class="left_menu_link" href="/ADMT">
		<div class="menu_item_logo">A</div><div class="menu_item_label">DMT</div>
	</a>
	<a class="left_menu_link" href="/Edit.php">
		<div class="menu_item_logo">E</div><div class="menu_item_label">ditor</div>
	</a>
	<a class="left_menu_link" href="/ADMT/users.php">
		<div class="menu_item_logo">U</div><div class="menu_item_label">sers</div>
	</a>
	<a class="left_menu_link" href="/ADMT/manufacturers.php">
		<div class="menu_item_logo">M</div><div class="menu_item_label">anufacturers</div>
	</a>
	<a class="left_menu_link" href="/ADMT/collections.php">
		<div class="menu_item_logo">C</div><div class="menu_item_label">ollections</div>
	</a>
	<a class="left_menu_link" href="/ADMT/items.php">
		<div class="menu_item_logo">I</div><div class="menu_item_label">tems</div>
	</a>
</div>