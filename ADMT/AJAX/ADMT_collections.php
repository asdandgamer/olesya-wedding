<?php /*
* file:		ADMT_collections.php @ ADMT : AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		24.09.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Site.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/ItemsManager.php';
//---------------------------------------------------------
$itemsManager = new ItemsManager();

if (isset($_GET["type"]) ) {
	if($_GET["type"]=='collections' && isset($_GET["category"])) {
		echo json_encode($itemsManager->GetCollectionsNamesByCategory($_GET["category"]));
	} else if($_GET["type"]=='collections' && isset($_GET["collectionID"])) {
		echo json_encode($itemsManager->GetFullCollectionInfo($_GET["collectionID"]));
	} else if($_GET["type"]=='categories') {
		echo json_encode($itemsManager->GetCategories());
	} else if($_GET["type"]=='manufacturers') {
		echo json_encode($itemsManager->GetManufacturersWID());
	}
}
else if(isset($_POST)) {
    if ($_FILES) {
        $path = 'IMG/COLLIMG/' . $_FILES['collImgFile']['name'];
        if(move_uploaded_file($_FILES['collImgFile']['tmp_name'], 
           $_SERVER["DOCUMENT_ROOT"] .'/'. $path))
            echo json_encode(array('status' => true, 'imgPath' => $path));
        else ReturnError('Сталася помилка при завантаженні картинки');
    } else if(isset($_POST["command"])) {
        switch ($_POST["command"]) {
            case 'addcoll': 
                $itemsManager->AddCollection(
                    $_POST["collName"], 
                    $_POST["collYear"],
                    $_POST["collCtg"],
                    $_POST["collMfg"],
                    $_POST["collImg"]
                );
                break;
            case 'delcoll':
                    $itemsManager->RemoveCollection($_POST["collID"]);
                break;
            default:
                ReturnError('Невідома команда.'.$_POST["command"]); return;
                break;
        }
        echo json_encode(array('status' => true));
    }
} else {
    ReturnError('Неправильне звернення.');
}

function ReturnError($reason) // print error reason in JSON 
{
    echo json_encode(array('status' => false, 'reason' => $reason));
}

?>