<?php /*
* file:		ADMT_items.php @ ADMT : AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		13.09.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Site.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/ItemsManager.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/ImageManager.php';
//---------------------------------------------------------
$itemsManager = new ItemsManager();
$imgMan = new ImagesManager();

if(isset($_GET["itemID"])) {
	$JSONarr = array();
	$itemPrev = $itemsManager->GetItemInfo($_GET['itemID']);
	array_push($JSONarr, array('id' => $itemPrev[0]));
	array_push($JSONarr, array('name' => $itemPrev[1]));
	array_push($JSONarr, array('coll' => $itemsManager->GetCollectionName($itemPrev[2])));
	array_push($JSONarr, array('price' => $itemPrev[3]));
	//-----------------------------------------------------
	$images = array();
	$imgIDs = explode(",", $itemPrev[4]);
	for ($i=0; $i < count($imgIDs); $i++) { 
		array_push($images, $imgMan->Gets200($imgIDs[$i]));
	}
	//-----------------------------------------------------
	array_push($JSONarr, array('imgLinks' => $images));
	array_push($JSONarr, array('autor' => $itemsManager->GetAutorByID($itemPrev[6])));
	
	// echo json_encode($itemsManager->GetItemInfo($_GET['itemID']));
	echo json_encode($JSONarr);
} else if(isset($_POST["command"])) {
	switch ($_POST["command"]) {
		case 'deleteitem':
				$itemsManager->RemoveItem($_POST["itemID"],
							$_POST["withImages"]);
			break;
		default:
			ReturnError('Невідома команда.'); return;
			break;
	}
	echo json_encode(array('status' => true));
}

function ReturnError($reason) // print error reason in JSON 
{
    echo json_encode(array('status' => false, 'reason' => $reason));
}

?>