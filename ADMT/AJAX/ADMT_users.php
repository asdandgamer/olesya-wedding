<?php /*
* file:		ADMT_users.php @ ADMT : AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		05.09.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once '../CORE/UserManager.php';


if(isset($_POST['command'])) {
	$code = 0;
	$usrMan = new UserManager();
	switch ($_POST['command']) {
		case 'adduser': ReturnResult($usrMan->AddUser(
				$_POST['login'], $_POST['pass'],$_POST['level']));
			break;
		case 'deleteuser': ReturnResult($usrMan->DeleteUser($_POST['userID']));
			break;
		case 'setpass': ReturnResult($usrMan->SetNewPassword(
			$_POST['userID'], $_POST['pass']));
			break;
		case 'setlvl': ReturnResult($usrMan->SetLevelToUser(
				$_POST['userID'], $_POST['level']));
			break;		
		default: ReturnError('Невідома команда!');
			break;
	}
} else {
    ReturnError('No command!');
}
function GetUserLevelName($lvl) {
	switch ($lvl) {
		case 'USR_LEVEL_GUEST': return 'Гість';
		case 'USR_LEVEL_USER': return 'Користувач';
		case 'USR_LEVEL_MODER': return 'Модератор';
		case 'USR_LEVEL_ADMIN': return 'Адміністратор';
		default: return 'Помилка!';
	}
}
function ReturnResult($code) {
	switch($code) {
		case SUCCESS: echo json_encode(array('status' => true)); break;
        case REG_YOU_NOT_ADMIN: ReturnError("Ви не являєтесь Адміністратором!"); break;
        case REG_BAD_USER_NAME: ReturnError("Невірний нікнейм"); break;
        case REG_BAD_USER_PASS: ReturnError("Поганиий пароль! Спробуйте інший."); break;
		case REG_USERNAME_EXIST: ReturnError("Користувач з таким нікнеймом уже існує!"); break;
		case WARN_DELETE_ADMIN: ReturnError("Ви не можете видалити адміністратора!"); break;
		case WARN_CHANGE_ADMIN_LEVEL: ReturnError("Ви не можете змінити права адміністратора!"); break;
        default: ReturnError('Невідома помилка!'); break;
    }
}

function ReturnError($reason) // print error reason in JSON 
{
    echo json_encode(array('status' => false, 'reason' => $reason));
}

?>