<?php /*
* file:		ADMT_userbar.php @ ADMT : AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		15.09.2018
*/ 
ini_set("display_errors", true);

require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Site.php';

if(isset($_POST["command"])) {
    switch($_POST["command"]) {
        case 'logout': Site::LogOut(); echo json_encode(array('status' => true));
            break;
        default: ReturnError("unknown command"); break;
    }
} else {
    ReturnError("FAILURE: No command!");
}

function ReturnError($reason) // print error reason in JSON 
{
    echo json_encode(array('status' => false, 'reason' => $reason));
}

?>