<?php /*
* file:		ADMT_items.php @ ADMT : AJAX
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		13.09.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Site.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/ItemsManager.php';
//---------------------------------------------------------
$itemsManager = new ItemsManager();

if(isset($_POST["command"])) {
	switch ($_POST["command"]) {
        case 'addmfg': 
            $itemsManager->AddManufacturer($_POST["mfgName"]);
            break;
		case 'delmfg':
				$itemsManager->RemoveManufacturer($_POST["mgfID"]);
			break;
		default:
			ReturnError('Невідома команда.'); return;
			break;
	}
	echo json_encode(array('status' => true));
}

function ReturnError($reason) // print error reason in JSON 
{
    echo json_encode(array('status' => false, 'reason' => $reason));
}

?>