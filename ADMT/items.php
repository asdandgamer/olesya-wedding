<?php /*
* file:	    items.php @ ADMT
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Site.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/ItemsManager.php';
//---------------------------------------------------------
$itemsManager = new ItemsManager();

if (!Site::getLogInStatus()) {
	Site::Redirect('/login.php?location=admt_items');
} else if(Site::GetCurrentUserLevel() < USR_LEVEL_MODER) {
	require_once 'TEMPLATE/Restricted.php';
} else {
	require_once 'TEMPLATE/ADMT_items.php';
}
//---------------------------------------------------------
function _DUMP($value)
{
	echo '<pre>';
	var_dump($value);
	echo '</pre>';
}

?>