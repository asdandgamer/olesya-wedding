/*
* file:		ADMT_users.js @ ADMT : JS
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		15.09.2018
*/

/*----------------------------------*/
// User Bar Funcs

function LogOut() {
    $.ajax({
		type: "POST",
		url: 'AJAX/ADMT_userbar.php',
		data: { command: "logout"},
		dataType: "json",
		success: function(data) {
			if(data.status == true) window.location.href = "/login.php";
			else alert(data.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			alert('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}