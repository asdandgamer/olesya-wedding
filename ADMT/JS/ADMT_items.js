/*
* file:		ADMT_items.js @ ADMT : JS
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		17.09.2018
*/

function EditItem(itemID) {
	window.location.href = "/ADMT/EditItem.php?itemID="+itemID;
}
//-------------------------------------------------------------------
function OpenAddItem() {
	window.location.href = "/Edit.php";
}
//-------------------------------------------------------------------
function OpenPreview(itemID) {
	$.ajax({
		type: "GET",
		url: "AJAX/ADMT_items.php",
		dataType: "json",
		data: { itemID: itemID },
		success: function(data) {
            AppendImages(data[4].imgLinks);
			$('#prevdebug').html(JSON.stringify(data));
			// var id = data[0].id;
			// var name = data[1].name;
            // alert(JSON.stringify(data[4]));
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
	//---------------------------------------------------------------
	$('#itemprev').show();
}
function AppendImages(srcArr) {
	$('.imgs_container').empty();
    for (let i = 0; i < srcArr.length; i++) {
        var img = document.createElement('img');
        img.id = 'img_'+i+1;
        img.src = srcArr[i];
		$('.imgs_container').append(img);
    }
}
function CloseItemPrev() { $('#itemprev').hide(); }
//-------------------------------------------------------------------
function OpenDeleteItem(itemID, itemName) {
	$('#delitemname').text(itemName);
	$('#delitembtn').removeAttr('onclick');
	$('#delitembtn').attr('onClick', 'DeleteItem('+itemID+')');
	//---------------------------------------------------------------
	$('#deleteitem').show();
}
function CloseDeleteItem() {
	$('#delImgs').attr('checked', false);
	$('.errtxt').empty();
	$('.errtxt').hide();
	$('#deleteitem').hide();
}
function DeleteItem(itemID) {
	var data = {
		command: 'deleteitem',
		itemID: itemID,
		withImages: $('#delImgs').is(":checked")
	};
	$.ajax({
		type: "POST",
		url: 'AJAX/ADMT_items.php',
		data: data,
		dataType: "json",
		success: function(data) {
			if(data.status == true) window.location.href = "/ADMT/items.php";
			else ShowError(data.reason);
		},
		error: function(xhr) {
			console.error(xhr);
			ShowError('Responce: ' + xhr.responseText + ';<br>Status:' + xhr.statusText);
		}
	});
}
//-------------------------------------------------------------------
function ShowError(html) {
	$('.errtxt').html(html);
	$('.errtxt').fadeIn();
}