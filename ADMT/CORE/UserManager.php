<?php /*
* file:		UserManager.php @ ADMT:CORE
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		01.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Site.php';
//----------------------------------------------------------------
define("SUCCESS", 0);

// define("USR_LEVEL_ADMIN", 9);
// define("USR_LEVEL_MODER", 4);
// define("USR_LEVEL_USER", 2);
// define("USR_LEVEL_GUEST", 1);

define("REG_YOU_NOT_ADMIN", 10);
define("REG_BAD_USER_NAME", 11);
define("REG_BAD_USER_PASS", 12);
define("REG_USERNAME_EXIST", 13);

define("WARN_DELETE_ADMIN", 20);
define("WARN_CHANGE_ADMIN_LEVEL", 21);

/* USAGE:
	$code = 0; // store returned code
	$usrMan = new UserManager();
	// set level to user
	$code = $usrMan->SetLevelToUser('1', USR_LEVEL_ADMIN);
	---------------------------------------------------------
	// add user 
	$code = $usrMan->AddUser('tolya', 'CPP123add', USR_LEVEL_ADMIN);
	---------------------------------------------------------
	// set user passwd
	$code = $usrMan->SetNewPassword('1', 'mode3DLL');
	---------------------------------------------------------
	// delete user
	$code = $usrMan->DeleteUser('2');
	---------------------------------------------------------
	// show code info
	if($code != SUCCESS) {
		switch($code) {
			case REG_YOU_NOT_ADMIN: echo "YOU NOT ADMIN"; break;
			case REG_BAD_USER_NAME: echo "Bad user name"; break;
			case REG_BAD_USER_PASS: echo "Bad passwd! Try another."; break;
			case REG_USERNAME_EXIST; echo "User with this name already exist!"; break;
			default ;
		}
	}
	---------------------------------------------------------
	// get list of all users
	_DUMP($usrMan->GetAllUsers()); 
*/
class UserManager
{
	private $db;
	
	function __construct()
	{
		$this->db = new DB();
		$this->db->Connect();
	}
	//---------------------------------------------------------------
	public function GetAllUsers()
	{
		$users = $this->db->Select('usr');
		//---------------------------------------------------------------
		// $this->_DUMP($users);
		return $users;
	}
	public function AddUser($usrName, $passwd, $level)
	{
		if($this->NotAdmin()) return REG_YOU_NOT_ADMIN;
		//-----------------------------------------------------------
		if (!preg_match('/^[a-z\d_]{2,20}$/i', $usrName))
			return REG_BAD_USER_NAME;
		if(!preg_match('/[A-Za-z0-9_#*]{8,32}$/i', $passwd))
			return REG_BAD_USER_PASS;
		//-----------------------------------------------------------
		if(count($this->db->Select('usr', 'id', 'name=\''.$usrName.'\''))>0) 
			return REG_USERNAME_EXIST;
		//-----------------------------------------------------------
		$userID = $this->GetLastId('usr')+1;
		$this->db->InsertVal('usr', array(	$userID, $usrName,    //!!!! FINISH IT !!!!!!!!!!
											md5(md5($passwd).$usrName),
											$level));
		// default---------------------------------------------------
		return SUCCESS;
	}
	public function DeleteUser($userID)
	{
		if($this->NotAdmin()) return REG_YOU_NOT_ADMIN;
		if(Site::GetUserLevelByID($userID) == USR_LEVEL_ADMIN)
			return WARN_DELETE_ADMIN;
		//-----------------------------------------------------------
		$this->db->Delete('usr', 'id='.$userID);
		return SUCCESS;
	}
	public function SetNewPassword($userID, $newPass)
	{
		if(!preg_match('/[A-Za-z0-9_#*]{8,32}$/i', $newPass))
			return REG_BAD_USER_PASS;
		//-------------------------------------------------
		$usrName = $this->db->Select('usr', 'name', 'id='.$userID)[0][0];
		$this->db->Update('usr', 'hash', md5(md5($newPass).$usrName), 'id='.$userID);
		return SUCCESS;
	}
	public function SetLevelToUser($userID, $newLevel)
	{
		if($this->NotAdmin()) return REG_YOU_NOT_ADMIN;
		if(Site::GetUserLevelByID($userID) == USR_LEVEL_ADMIN)
			return WARN_CHANGE_ADMIN_LEVEL;
		//-----------------------------------------------------------
		$this->db->Update('usr', 'level', $newLevel, 'id='.$userID);
		return SUCCESS;
	}
	//---------------------------------------------------------------
	private function GetLastId($tableName)
	{
		$records = $this->db->Select($tableName);
		if(count($records) == 0)
			return 0;
		else
			return (int)$records[count($records)-1][0];
		//$this->_DUMP((int)$records[count($records)-1][0]);
	}
	//---------------------------------------------------------------
	private function NotAdmin()
	{
		if(Site::GetCurrentUserLevel() < USR_LEVEL_ADMIN)
			return REG_YOU_NOT_ADMIN;
		else
			return 0;
	}
	private function _DUMP($value)
	{
		echo '<pre>';
		var_dump($value);
		echo '</pre>';
	}
}


?>