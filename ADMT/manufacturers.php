<?php /*
* file:		manufacturers.php @ ADMT
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		17.09.2018
* ---------------------------------------------------------------- */
ini_set("display_errors", true); // !!! remove after DEBUG !!!
require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Engine.php';
require_once 'CORE/UserManager.php';

if (!Site::getLogInStatus()) {
    Site::Redirect('/login.php?location=admt');
} else if(Site::GetCurrentUserLevel() < USR_LEVEL_ADMIN) {
	require_once 'TEMPLATE/Restricted.php';
} else {
    require_once 'TEMPLATE/ADMT_manufacturers.php';
}

?>