<?php /*
* file:		index.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		07.02.2018
*/ 
	ini_set('display_errors', true); // !!! remove after DEBUG !!!
//-------------------------------------------------------------------
$start_time = microtime();
// разделяем секунды и миллисекунды (становятся значениями начальных ключей массива-списка)
$start_array = explode(" ",$start_time);
// это и есть стартовое время
$start_time = $start_array[1] + $start_array[0];
//-------------------------------------------------------------------
	/*site settings TODO*/
	require_once 'ENGINE/Engine.php';
	require_once 'VisitorsInfo.php';
$Title = Site::GetSiteName();
$description = 'Салон весільного та вечірнього вбрання &quot;OLESYA&quot; в ЦУМ\'і для досвідчених Тернопільських модниць. Весільні сукні Олеся Тернопіль.';
// Open Graph -------------------------------------------------------
$OG = array(
  'title' => $Title,
  'url' => 'https://olesya-wedding.com.ua/',
  'description' => $description,
  'image' => 'https://olesya-wedding.com.ua/img/logo.png'
);

include_once 'TEMPLATE/header.php';	//header
include_once 'TEMPLATE/menu.php';	//menu
include_once 'TEMPLATE/main.php';	//main page
include_once 'TEMPLATE/footer.php';	//footer

//-------------------------------------------------------------------
// делаем то же, что и в start.php, только используем другие переменные
$end_time = microtime();
$end_array = explode(" ",$end_time);
$end_time = $end_array[1] + $end_array[0];
// вычитаем из конечного времени начальное
$time = $end_time - $start_time;
if (Site::getLogInStatus()) {
	// выводим в выходной поток (броузер) время генерации страницы
	printf("Страница сгенерирована за %f секунд",$time);
}
//-------------------------------------------------------------------

?>