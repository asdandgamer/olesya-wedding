<?php /*
* file:		video.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		27.02.2018
*/ 
	ini_set('display_errors', true); // !!! remove after DEBUG !!!
	// --------------------------------------------------------------
	require_once 'ENGINE/Engine.php';
	require_once 'VisitorsInfo.php';
$label = 'Відео';
$Title = $label . ' | ' . Site::GetSiteName();
$description = 'Салон весільного та вечірнього вбрання &quot;OLESYA&quot; в ЦУМ\'і для досвідчених Тернопільських модниць. Olesya Wedding Тернопіль.';

include_once 'TEMPLATE/header.php';	//header
include_once 'TEMPLATE/menu.php';	//menu
include_once 'TEMPLATE/video.php';	//main page
include_once 'TEMPLATE/footer.php';	//footer

?>