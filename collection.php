<?php /*
* file:		item.php
* autor:	asdandgamer
* e-mail:	asdandgamer@gmail.com
* date:		08.02.2018
*/
ini_set('display_errors', true); // !!! remove after DEBUG !!!
require_once 'ENGINE/Engine.php';

$itemsManager = new ItemsManager();
$items = array();
$collectiomName = '';
$collectionFirstImgId = 0;
$categoryInf = array();
if (isset($_GET['CollID'])) {
  $options = array(
      'options' => array(
          'default' => 0, // значение, возвращаемое, если фильтрация завершилась неудачей
          // другие параметры
          'min_range' => 0
      ),
      'flags' => FILTER_FLAG_ALLOW_OCTAL,
  );
  $var = filter_var($_GET['CollID'], FILTER_VALIDATE_INT, $options);
  if ($var == 0) {
    Site::Redirect('/404');
  }
  //----------------------------------------------------------
  array_push($categoryInf, $itemsManager->GetCollectionInfo($_GET['CollID'])[3]);
  array_push($categoryInf, $itemsManager->GetCategopryName($categoryInf[0]));
	$collectiomName = $itemsManager->GetCollectionName($_GET['CollID']);
	$items = $itemsManager->GetItemsByCollection($_GET['CollID']);
} else {
	Site::Redirect('404.html');
}
$Title = $collectiomName . ' | ' . Site::GetSiteName();

function getFirstCollectionImg($items, $itemIndex) {
	return explode(', ', $items[$itemIndex][4])[0];
}
//-------------------------------------------------------------------
include_once 'TEMPLATE/header.php';	//header
include_once 'TEMPLATE/menu.php';	//menu
//include_once 'TEMPLATE/category.php';	//category
//-------------------------------------------------------------------
echo '<div class="breadcrumbs">' . 
        '<a href="/"><img src="img/home.png"></a> &gt; ' . 
        '<a href="/category.php?CatID=' . $categoryInf[0] . '">' . $categoryInf[1] . '</a> &gt; ' . $collectiomName . 
     '</div>';
echo '<div class="content_wrap">
    <div class="product_categories">
    <div class="text">' . $collectiomName . '</div>';
    for ($i=0; $i < count($items); $i++) { 

    	echo '<a href="/item.php?itemID=' . $items[$i][0] . '">';
    	echo '<div id="' . $items[$i][0] . '" class="product_category"> 
          <div class="label">  <span>'.$items[$i][1] . '</span>
          </div>
          <div id="' . getFirstCollectionImg($items, $i) . '" class="image">
            <img src="">
          </div>
        </div>';
    	echo '</a>';
    }
echo '</div>
  </div>';
echo '<script src="JS/collection.js"></script>';
//-------------------------------------------------------------------
include_once 'TEMPLATE/footer.php';	//footer
?>